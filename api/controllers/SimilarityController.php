<?php

namespace app\api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\db\Query;

class SimilarityController extends \yii\rest\Controller {

    protected function verbs() {
        return [
            'check' => ['POST'],
        ];
    }

    public function actionCheck() {
        $error = NULL;
        if (Yii::$app->request->getBodyParams()) {
            $text1 = Yii::$app->request->getBodyParams()['text1'];
            $text2 = Yii::$app->request->getBodyParams()['text2'];
            $test = $this->testPlagiarism($text1, $text2);
            $response["similarity"] =  $test["similarity"];
            $response["result"] = $test["text_diff"];
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Failed";
        }
        echo json_encode($response);
    }

    protected function testPlagiarism($string1,$string2) {
        $a = '';
        $granularity = new \cogpowered\FineDiff\Granularity\Word;
        $renderer = new \cogpowered\FineDiff\Render\Html;
        $diff = new \cogpowered\FineDiff\Diff($granularity, $renderer);
        $render = $diff->render($string1, $string2);
        preg_match_all('@' . Yii::$app->params['prependTag'] . '(.*?)' . Yii::$app->params['appendTag'] . '@si', $render, $array);
        $count_match = 0;
        foreach ($array[1] as $value) {
            $count_match += str_word_count(trim($value));
            $a .= $value;
        }

        $count_words = str_word_count(trim($string1));
        $result['text_diff']=$render;
        $result['similarity']=round($count_match / $count_words * 100, 2);

        return $result;
    }

    public function behaviors() {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];

        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        return $behaviors;
    }

}
