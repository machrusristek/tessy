<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LpAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/tessy.css',
        'css/popup.css',
//        'css/site.css',
//        'css/tessy-css/brainly.css',
//        'css/tessy-css/careers.css',
//        'css/tessy-css/default.css',
//        'css/tessy-css/eyyqrnehtj.css',
//        'css/tessy-css/following.css',
//        'css/tessy-css/index.css',
//        'css/tessy-css/mue8rgetke.css',
//        'css/tessy-css/twitter2bingtranslator.css',
//        'css/tessy-css/814639084622577668.css',
//        'css/tessy-css/814820919361224704.css',
//        'css/tessy-css/814833567624790016.css',
        
    ];
    public $js = [
       
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
