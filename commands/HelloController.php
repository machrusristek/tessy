<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use \app\models\Literature;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }

    public function actionTes(){
        $model=Literature::find()->all();
        $dir = \Yii::getAlias('@app/web/file');
        foreach($model as $data){
            $file=$dir."/".$data->title.".pdf";
            if(file_exists($file)){
                echo $data->title." Ada". "\n";
                $hasil=Literature::pdfTotext($file);
                if(!$hasil){
                    $data->content=$hasil;
                    $data->save();
                }else{
                    echo "GAGAL SAVE". "\n";
                }
            }else{
                echo $data->title." Tidak ada". "\n";
            }
            // echo $dir; 
        }
    }
}
