<?php

return [
    'imageUrl' => '',
    'adminEmail' => 'aman.rohiman@gmail.com',
    'prependTag' => '<p class="text-red">',
    'appendTag' => '</p>',
    'minimumWords' => 4,
    'maxComparison' => 15,
    'studentSelfTest' => true,
    'matchOnlyRepo'=>true,
    'maxFileSize' => 50, //MB
    'authMode'=>'Web', // LDAP | Mixed | Web
    'serviceURL'=>'http://etd.repository.ugm.ac.id/service/index.php?mod=soap_gateway&sub=CentralGateway&act=Soap&typ=soap&wsdl',
    'serviceFunction'=>'Penelitian_GetListPenelitian',
    //'getListService'=>'http://192.168.254.203/~didi/gtpla_service/index.php?func=GetList&key=',
    'nfs'=>'/media/perpus',
    'extractMaxPage'=>300,
    'pdfBin'=>'/win/bin64',
    'thresold'=>40
];
