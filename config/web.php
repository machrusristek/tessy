<?php

Yii::setAlias('@bin', realpath(__DIR__ . '/../bin'));
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'gtPlagiarismTest',
    'defaultRoute' => 'login',
    'components' => [
        'opengraph' => [
            'class' => 'dragonjet\opengraph\OpenGraph',
        ],
        'googleApi' =>
        [
            'class' => '\skeeks\yii2\googleApi\GoogleApiComponent',
            'developer_key' => 'AIzaSyDx4BH7bcF8DKDr8YfAcy_Ji01nFO2jpjU',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/views/layouts'
                ],
            ],
        ],
        'authManager' => [
            'class' => 'letyii\rbacmongodb\MongodbManager',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '1uWHJOI-rGsG-VBOZ7gVmTOW7I-Yojej',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                // 'facebook' => [
                //     'class' => 'yii\authclient\clients\Facebook',
                //     //'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                // //    'clientId' => '505515489609294',
                // //    'clientSecret' => 'ff861145bcb393407b4e3377d4b6b9df',
                //     'clientId' => '260958364335464',
                //     'clientSecret' => '65cd11ba4b3a0017d300a61c61830331',
                // ],
//                'google' => [
//                    'class' => 'yii\authclient\clients\Google',
//                    //'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
////                    'clientId' => '505515489609294',
////                    'clientSecret' => 'ff861145bcb393407b4e3377d4b6b9df',
//                    'clientId' => '1019156991265-ckasjt9ukiaufccta1u9fdlmiptpk36u.apps.googleusercontent.com',
//                    'clientSecret' => 'eqkytQ_LPPHo-QKMq11rUiVJ',
//                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'tessy.idn@gmail.com',
                'password' => 'b!smill4h',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                ],
            ],
        ],
    //    'db' => require(__DIR__ . '/db.php'),
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            //'dsn' => 'mongodb://developer:bismillah@35.164.36.198:27090/tessy',
            'dsn' => 'mongodb://localhost:27017/tessy-simlitabmas',
            'enableLogging' => true, // enable logging
            'enableProfiling' => true, // enable profiling
        ],
        'session' => [
            'class' => 'yii\mongodb\Session',
        ],
//        'cache' => [
//            'class' => 'yii\mongodb\Cache',
//        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\mongodb\i18n\MongoDbMessageSource'
                ]
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '/' => 'site/login',
                '/home' => 'site/home',
                '/login' => 'site/login',
                '/verification' => 'site/verification',
                '/register' => 'site/register',
                '/validate' => 'site/validate',
                '/news' => 'news/all',
                'group/<slug>' => 'group/room'
            ],
        ],
    ],
//     'as beforeRequest' => [
//     'class' => 'yii\filters\AccessControl',
//     'rules' => [
//         [
//             'allow' => true,
//             'actions' => ['/', 'site/index', 'home','login','register','logout','validate','single','all','auth', 'login-home', 'request-password-reset','similarity/check'],
//             'roles' => ['@'],
//         ],
//         [
//             'allow' => true,
//             'roles' => ['?'],
//         ],
//     ],
    
// ],
    'modules' => [
        'api' => [
            'basePath' => '@app/api', // base path for our module class
            'class' => 'app\api\Api', // Path to module class
        ],
        'social' => [
            // the module class
            'class' => 'kartik\social\Module',
            // the global settings for the Disqus widget
            'disqus' => [
                'settings' => ['shortname' => 'tessy-id'] // default settings
            ],
            // the global settings for the Facebook plugins widget
            'facebook' => [
                'appId' => 'FACEBOOK_APP_ID',
                'secret' => 'FACEBOOK_APP_SECRET',
            ],
            // the global settings for the Google+ Plugins widget
            'google' => [
                'clientId' => 'GOOGLE_API_CLIENT_ID',
                'pageId' => 'GOOGLE_PLUS_PAGE_ID',
                'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
            ],
            // the global settings for the Google Analytics plugin widget
            'googleAnalytics' => [
                'id' => 'TRACKING_ID',
                'domain' => 'TRACKING_DOMAIN',
            ],
            // the global settings for the Twitter plugin widget
            'twitter' => [
                'screenName' => 'TWITTER_SCREEN_NAME'
            ],
            // the global settings for the GitHub plugin widget
            'github' => [
                'settings' => ['user' => 'GITHUB_USER', 'repo' => 'GITHUB_REPO']
            ],
        ],
        'blog' => [
            'id' => 'blog',
            'class' => devmustafa\blog\modules\backend\Module::className(),
            'upload_url' => 'https://tessy.id/uploads', // full upload url
            'upload_directory' => dirname(__DIR__) . '/web/uploads', // full upload directory
            'used_languages' => ['en'], // list of languages used
            'default_language' => 'en', // default language
        ],
        'blogs' => [
            'id' => 'blog',
            'class' => devmustafa\blog\modules\frontend\Module::className(),
            'used_languages' => ['en'], // list of languages used
            'default_language' => 'en', // default language
            'listing_size' => 1, // default size of listing page
            'rules' => [ // setup rules for frontend routes
                // change key not value
                'posts' => '/blogs/post',
                'post/<slug>' => '/post/single',
                'category/<slug>' => '/post/category', // posts related to a specific category
            ]
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'panels' => [
            'db' => [
                'class' => 'yii\\mongodb\\debug\\MongoDbPanel',
                // 'db' => 'mongodb',
            ],
        ],
        'allowedIPs' => ['*']
];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'mongoDbModel' => [
                'class' => 'yii\mongodb\gii\model\Generator'
            ]
        ],
    ];
}

return $config;
