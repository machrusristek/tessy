<?php

namespace app\controllers;

use Yii;
use app\models\Adhoc;
use app\models\AdhocSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Asset;

/**
 * AdhocController implements the CRUD actions for Adhoc model.
 */
class AdhocController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Adhoc models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AdhocSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Adhoc model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Adhoc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Adhoc();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->method == 'File') {
                $model->file1 = UploadedFile::getInstance($model, 'file1');
                $model->file2 = UploadedFile::getInstance($model, 'file2');

//                if ($model->file1 && $model->file2 && $model->validate()) {
                    $model->file1->saveAs(Yii::getAlias('@webroot') . '/uploads/' ."1-".$model->file1);
                    $model->file2->saveAs(Yii::getAlias('@webroot') . '/uploads/' . "2-".$model->file2);
//                }
            }
            
            if ($model->save(false)) {
                if ($model->method == 'File') {
                    $filename1 = Yii::getAlias('@webroot') . '/uploads/' ."1-". $model->file1;
                    $asset1 = new Asset();
                    $asset1->adhoc_id = (string) $model->_id.'1';
                    $asset1->literature_id = '';
                    $asset1->file = $filename1;
                    $asset1->contentType = $model->file1->type;
                    $asset1->filename = "1-". $model->file1;
                    $asset1->uploadDate = date('Y-m-d h:i:s');
                    $asset1->save();
                    
                    $filename2 = Yii::getAlias('@webroot') . '/uploads/' ."2-". $model->file2;
                    $asset2 = new Asset();
                    $asset2->adhoc_id = (string) $model->_id.'2';
                    $asset2->literature_id = '';
                    $asset2->file = $filename2;
                    $asset2->contentType = $model->file2->type;
                    $asset2->filename = "2-". $model->file2;
                    $asset2->uploadDate = date('Y-m-d h:i:s');
                    $asset2->save();
                    unlink(Yii::getAlias('@webroot') . '/uploads/' ."1-". $model->file1);
                    unlink(Yii::getAlias('@webroot') . '/uploads/' ."2-". $model->file2);
                }
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Adhoc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Adhoc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Adhoc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Adhoc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Adhoc::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
