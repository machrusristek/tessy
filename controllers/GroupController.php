<?php

namespace app\controllers;

use Yii;
use app\controllers\SiteController;
use app\models\Asset;
use app\models\Group;
use app\models\GroupCompare;
use app\models\GroupCompareSearch;
use app\models\GroupMember;
use app\models\GroupMemberSearch;
use app\models\GroupSearch;
use app\models\User;
use yii\base\ErrorException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends SiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new GroupMemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        $searchModelCompare = new GroupCompareSearch();
        $dataProviderCompare = $searchModelCompare->search(Yii::$app->request->queryParams, $id);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelCompare' => $searchModelCompare,
            'dataProviderCompare' => $dataProviderCompare
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRoom($slug, $tgl_deadline){
        //echo $tgl_deadline;die;
        $model = Group::findOne(['slug' => $slug, 'deadline' => date('Y-m-d H:i', strtotime($tgl_deadline))]);
        
        $groupMember = new GroupMember();

        $teacherName = User::findOne($model->supervisor_id);

        if($groupMember->load(Yii::$app->request->post())){
            try{
                GroupMember::deleteAll(['class_id' => (string)$model->_id,'student_id' => (string)Yii::$app->user->identity->id]);
                Asset::deleteAll(['group_id' => (string)$model->_id, 'user_id' => (string)Yii::$app->user->identity->id]);

                $document = \yii\web\UploadedFile::getInstance($groupMember, 'document');
                $document->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $document);

                $filepath = Yii::getAlias('@webroot') . '/uploads/' . $document;

                $content = $this->pdftotext($filepath);

                $groupMember->class_id = (string)$model->_id;
                $groupMember->student_id = (string)Yii::$app->user->identity->id;
                if ($content == '' || substr($content, 0, 5) == 'ERROR') {
                    Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
                    throw new \yii\web\HttpException(405);
                } else {
                    $groupMember->document = $content;
                }
                $groupMember->created_time = date('Y-m-d H:i:s');
                $groupMember->save();
                if(!$groupMember->getErrors()){
                    $asset = new Asset();
                    $asset->class_id = (string)$model->_id;
                    $asset->user_id = (string)Yii::$app->user->identity->id;
                    $asset->file = $filepath;
                    $asset->contentType = $document->type;
                    $asset->filename = $document->name;
                    $asset->uploadDate = date('Y-m-d h:i:s');
                    $asset->save();
                }
                Yii::$app->session->setFlash('success', "Document uploaded");
            }catch(ErrorException $e){
                echo $e;
            }


        }

        return $this->render('room',[
            'model' => $model,
            'teacherName' => $teacherName,
            'groupMember' => $groupMember
        ]);
    }

    /**
     * find file in asset collection
     * @param  string $id      [description]
     * @param  string $user_id [description]
     * @return [type]          [description]
     */
    public function actionFile($id, $user_id) {
        $asset = Asset::findOne(['class_id' => $id, 'user_id' => $user_id]);

        return $this->renderAjax('_file', [
            'asset' => $asset,
        ]);
    }

    /**
     * display file in asset collection
     * @param  [type] $id      [description]
     * @param  [type] $user_id [description]
     * @return [type]          [description]
     */
    public function actionGet($id, $user_id) {
        $model = Asset::findOne(['class_id' => $id, 'user_id' => $user_id]);
        header('Content-type: ' . $model->contentType);
        echo $model->file->getBytes();
    }

    /**
     * action compare all document
     * @param type $id
     * @return json
     */
    public function actionCompare($id)
    {
        $granularity = new \cogpowered\FineDiff\Granularity\Word;
        $renderer = new \cogpowered\FineDiff\Render\Html;
        $diff = new \cogpowered\FineDiff\Diff($granularity, $renderer);

        //delete all group_id
        GroupCompare::deleteAll(['group_id' => $id]);

        $groupMember = GroupMember::find()->where(['class_id' => $id])->asArray()->all();

        $result = 'false';
        for($i=0;$i<count($groupMember);$i++){
            $student_id = $groupMember[$i]['student_id'];
            $document = $groupMember[$i]['document'];
            //echo $document;
            for($j=0;$j<count($groupMember);$j++){

                if($groupMember[$i] == $groupMember[$j]){
                    continue;
                }else{

                    $render = $diff->render($document, $groupMember[$j]['document']);
                    preg_match_all('@'.Yii::$app->params['prependTag'].'(.*?)'.Yii::$app->params['appendTag'].'@si', $render, $array);
                    $count_match = 0;

                    foreach ($array[1] as $value) {
                        $count_match += str_word_count(trim($value));

                    }
                     $count_words = str_word_count(trim($groupMember[$j]['document']));

                    $groupCompare = new GroupCompare();
                    $groupCompare->group_id = $id;
                    $groupCompare->tester = $groupMember[$j]['student_id'];
                    $groupCompare->testee = $student_id;
                    $groupCompare->diff = $render;
                    $groupCompare->score = round($count_match / $count_words * 100, 2);
                    $groupCompare->save();
                    $result = 'true';
                }

            }
        }
        return json_encode(['result' => $result]);
    }

    public function actionDiff()
    {
        $id = $_GET['id'];
        $tester_id = $_GET['tester_id'];
        $class_id = $_GET['class_id'];

        $diff = GroupCompare::findOne($id);
        $origin = GroupMember::findOne(['student_id' => $tester_id, 'class_id' => $class_id]);

        return $this->renderAjax('_diff',[
          'diff' => $diff,
          'origin' => $origin
        ]);
    }
}
