<?php

namespace app\controllers;

use Yii;
use app\models\Literature;
use app\models\LiteratureSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Asset;
use app\models\Keyword;
use Da\QrCode\QrCode;
use app\models\PlagiarismTest;
use app\models\PlagiarismCompare;
use keltstr\simplehtmldom\SimpleHTMLDom;
use yii\base\ErrorException;
use yii\mongodb\Query;
use yii\httpclient\Client;
use yii\helpers\Json;

/**
 * LiteratureController implements the CRUD actions for Literature model.
 */
class LiteratureController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Literature models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new LiteratureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(["create_time"=> SORT_DESC]);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Literature model.
     * @param integer $_id
     * @return mixed
     */
    public function actionGet($id) {
        $model = Asset::findOne($id);
        header('Content-type: ' . $model->contentType);
        echo $model->file->getBytes();
    }

    public function actionView($id) {

        $asset = Asset::findOne(['literature_id' => $id]);
        $model=$this->findModel($id);
        return $this->render('view', [
                    'model' => $model,
                    'asset' => $asset,
        ]);
    }

    public function actionKeywordSearch($q = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $querys = Keyword::find()->select(['name'])
                    ->where(['like', 'name', $q])
                    ->limit(20)
                    ->all();
            $data = array();
            foreach ($querys as $query) {
                $data['id'] = $query->name;
                $data['text'] = $query->name;
            }

            $out['results'] = [$data];
//        } elseif ($id > 0) {
//            $out['results'] = ['id' => $id, 'text' => Keyword::find($id)->name];
        }
        return $out;
    }

    function pdftotext($file) {
        try {
            $content = shell_exec(Yii::getAlias('@bin') . Yii::$app->params['pdfBin'] . '/pdftotext -nopgbrk -q -layout "' . $file . '" -');
            return utf8_encode($content);
        } catch (Exception $e) {
            return 'ERROR:' . $e->getMessage();
        }
    }

    public function actionPlagiarismCertificate($id) {
        return $this->render('certificate', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionFile($id) {
        $asset = Asset::findOne(['literature_id' => $id]);
        return $this->renderAjax('_file', [
                    'model' => $this->findModel($id),
                    'asset' => $asset,
        ]);
    }

    public function actionPlagiarismResult($id) {
        $test=  PlagiarismTest::findOne($id);
        $model= $this->findModel($test->document_id);
        if ($model->load(Yii::$app->request->post()) and $model->save()) {
            Yii::$app->session->setFlash('success', 'Status Saved');
        }
        return $this->render('result', [
                    'model' => $model,
                    'test' => $test,
        ]);
    }

    public function actionCreatePembanding() {
        $model = new Literature();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $file = \yii\web\UploadedFile::getInstance($model, 'file');
            //            var_dump($file);exit();
            $file->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $file);
            $filepath = Yii::getAlias('@webroot') . '/uploads/' . $file;
            $content = $this->pdftotext($filepath);
            if ($content == '' || substr($content, 0, 5) == 'ERROR') {
                Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
                throw new \yii\web\HttpException(405);
            } else {
                $model->konten = $content;
            }
            $model->pembanding = "Y";
            $model->file = $file->name;
            $model->author_id = Yii::$app->user->identity->id;
            $model->plagiarism_check_status = 'Submit';
            $model->keyword = $model->keyword != null ? implode(',', $model->keyword) : '';
            $programHibah=\app\models\ProgramHibah::findOne(['kode_program_hibah' =>  $model->kd_program_hibah]);
            $model->jenis_kegiatan=$programHibah?$programHibah->jenis_kegiatan:"";
            // var_dump($model->jenis_kegiatan);exit;
            $model->save();
            //            var_dump($model->getErrors());exit();
            if (!$model->getErrors()) {
                $id=(string) $model->_id;
                $asset = new Asset();
                $asset->literature_id = $id;
                $asset->adhoc_id = '';
                $asset->file = $filepath;
                $asset->contentType = $file->type;
                $asset->filename = $file->name;
                $asset->uploadDate = date('Y-m-d h:i:s');
                $asset->save();
                unlink(Yii::getAlias('@webroot') . '/uploads/' . $file);

                Yii::$app->getSession()->setFlash('success', 'Data Saved');
                return $this->redirect(['create-pembanding']);
            }else{
                Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreate() {
        $model = new Literature();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $file = \yii\web\UploadedFile::getInstance($model, 'file');
            //            var_dump($file);exit();
            $file->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $file);
            $filepath = Yii::getAlias('@webroot') . '/uploads/' . $file;
            $content = $this->pdftotext($filepath);
            if ($content == '' || substr($content, 0, 5) == 'ERROR') {
                Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
                throw new \yii\web\HttpException(405);
            } else {
                $model->konten = $content;
            }
            $model->pembanding = "N";
            $model->file = $file->name;
            $model->author_id = Yii::$app->user->identity->id;
            $model->plagiarism_check_status = 'Submit';
            $model->keyword = $model->keyword != null ? implode(',', $model->keyword) : '';
            $programHibah=\app\models\ProgramHibah::findOne(['kode_program_hibah' =>  $model->kd_program_hibah]);
            $model->jenis_kegiatan=$programHibah?$programHibah->jenis_kegiatan:"";
            // var_dump($model->jenis_kegiatan);exit;
            $model->save();
            //            var_dump($model->getErrors());exit();
            if (!$model->getErrors()) {
                $id=(string) $model->_id;
                $asset = new Asset();
                $asset->literature_id = $id;
                $asset->adhoc_id = '';
                $asset->file = $filepath;
                $asset->contentType = $file->type;
                $asset->filename = $file->name;
                $asset->uploadDate = date('Y-m-d h:i:s');
                $asset->save();
                unlink(Yii::getAlias('@webroot') . '/uploads/' . $file);

                return $this->redirect(['plagiarism-test/create', 'id' => (string) $model->_id]);
                // return $this->redirect(['index']);
            }else{
                Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionProcess($id) {
        $model = $this->findModel($id);
        $model->plagiarism_check_status="Processing";
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Literature model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $asset = Asset::findOne(['literature_id' => $id]);
        $s = explode(',', $model->keyword);
        foreach ($s as $d) {
            $data[] = $d;
        }
        $model->keyword = $data;
        if ($model->load(Yii::$app->request->post())) {
            $file = \yii\web\UploadedFile::getInstance($model, 'file');
            //            var_dump($file);exit();
            if($file){
              $file->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $file);
              $filepath = Yii::getAlias('@webroot') . '/uploads/' . $file;
              $content = $this->pdftotext($filepath);
              if ($content == '' || substr($content, 0, 5) == 'ERROR') {
                  Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
                  throw new \yii\web\HttpException(405);
              } else {
                  $model->content = $content;
              }
              $model->file = $file->name;
              $model->keyword = $model->keyword != null ? implode(',', $model->keyword) : '';
                $programHibah=\app\models\ProgramHibah::findOne(['kode_program_hibah' =>  $model->kd_program_hibah]);
                $model->jenis_kegiatan=$programHibah?$programHibah->jenis_kegiatan:"";
              $model->save();
              if (!$model->getErrors()) {
                  $asset->file = $filepath;
                  $asset->contentType = $file->type;
                  $asset->filename = $file->name;
                  $asset->uploadDate = date('Y-m-d h:i:s');
                  $asset->save();
                //   return $this->redirect(['plagiarism-test/create', 'id' => (string) $model->_id]);
                return $this->redirect(['index']);
              }
            }else{
              $model->keyword = $model->keyword != null ? implode(',', $model->keyword) : '';
              $model->save();
            //   return $this->redirect(['plagiarism-test/create', 'id' => (string) $model->_id]);
            return $this->redirect(['index']);
            }

        } else {
            return $this->render('update', [
                        'model' => $model,
                        'asset' => $asset,
            ]);
        }
    }

    /**
     * Deletes an existing Literature model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionQrcode($id) {
        $qrCode = (new QrCode(Yii::$app->request->hostInfo . '/validate?id=' . $id))
    ->setSize(100)
    ->setMargin(5);
    
    $filepath = Yii::getAlias('@webroot') . '/uploads/qrcode';
    $qrCode->writeFile($filepath."/$id.png"); 
    return '<img src="/uploads/qrcode/'.$id.'.png">';// writer defaults to PNG when none is specified
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        PlagiarismCompare::deleteAll(['literature_id' => $id]);
        PlagiarismTest::deleteAll(['document_id' => $id]);
        Asset::deleteAll(['literature_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Literature model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Literature the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Literature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
