<?php

/**
 * frontend - posts
 *
 * @author Mustafa Magdi <developer.mustafa@gmail.com>
 * @link https://github.com/devmustafa/yii2-blog-mongodb
 */

namespace app\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use yii\data\Pagination;
use devmustafa\blog\models\Post;

/**
 * Site controller
 */
class NewsController extends Controller
{
    /**
     * @var object
     */
//    public $module;

    /**
     * @inheritdoc
     */
//    public function init()
//    {
//        parent::init();
//        $this->module = Yii::$app->getModule('blogs');
//    }

    /**
     * list of posts
     * @return object view of posts list
     */
    public function actionAll()
    {
//        var_dump();exit();
        // get page
        $limit = 10;
        $page_number = Yii::$app->request->get('page', 1);
        $offset = $page_number - 1; // get offset

        $q = Yii::$app->request->get('q', '');

        // return query object
        $query = (new Post)->getPostsList($offset, $limit, $q);

        $posts = $query->all();

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'defaultPageSize' => $limit,
            'pageSize' => $limit,
            'route' => '/news',
        ]);
         //popular
        $queri = (new Post)->getPostsList('', 4, '');
        $popular = $queri->orderBy(['views'=>SORT_DESC])->all();
        return $this->render('index', [
            'posts' => $posts,
            'pages' => $pages,
            'popular' => $popular,
        ]);
    }

    /**
     * single post based on the slug passed
     * @return object view of single post
     */
    public function actionSingle()
    {
        // get slug
        $slug = Yii::$app->request->get('slug');

        // return query object
        $query = (new Post)->getSinglePost($slug);

        $post = $query->one();
        $news= Post::findOne($post->getId());
        $news->views = $news->views+1;
        $news->publish_date = date('Y-m-d', $news->publish_date);
        $news->save();
        if($post === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        //popular
        $queri = (new Post)->getPostsList('', 4, '');
        $popular = $queri->orderBy(['views'=>SORT_DESC])->all();
        return $this->render('single', [
            'post' => $post,
            'popular' => $popular,
        ]);
    }

    /**
     * single post based on the slug passed
     * @return object view of single post
     */
    public function actionCategory()
    {
        // get page
        $limit = $this->module->listing_size;
        $page_number = Yii::$app->request->get('page', 1);
        $offset = $page_number - 1; // get offset

        $slug = Yii::$app->request->get('slug', '');

        // return query object
        $query = (new Post)->getPostsByCategory($offset, $limit, $slug);

        $posts = $query->all();

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'defaultPageSize' => $limit,
            'pageSize' => $limit,
            'route' => '/category',
        ]);

        return $this->render('index', [
            'posts' => $posts,
            'pages' => $pages,
        ]);
    }

}
