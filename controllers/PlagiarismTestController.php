<?php

namespace app\controllers;

use Yii;
use app\models\PlagiarismCompare;
use app\models\Literature;
use app\models\PlagiarismTest;
use app\models\PlagiarismTestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use keltstr\simplehtmldom\SimpleHTMLDom;
use yii\base\ErrorException;
use yii\mongodb\Query;
use yii\httpclient\Client;
use yii\helpers\Json;

/**
 * PlagiarismTestController implements the CRUD actions for PlagiarismTest model.
 */
class PlagiarismTestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PlagiarismTest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlagiarismTestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // if(Yii::$app->user->identity->role == 'Reviewer'){
        //     $literature=Literature::find()->select(['_id'])->where(["review_id" => Yii::$app->user->identity->id,status=>"Tested"])->column();
        //     $data=implode("','",$literature);
        //     $data=explode("','",$data);
        //     $dataProvider->query->andWhere(['in', 'document_id',$data]);
        // }
        if(Yii::$app->user->identity->role == 'Applicant'){
            $dataProvider->query->andWhere(['=', 'submit_by', Yii::$app->user->identity->id]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    protected function compare($string1,$string2) {
        $a = '';
        $granularity = new \cogpowered\FineDiff\Granularity\Word;
        $renderer = new \cogpowered\FineDiff\Render\Html;
        $diff = new \cogpowered\FineDiff\Diff($granularity, $renderer);
        $render = $diff->render($string1, $string2);
        preg_match_all('@' . Yii::$app->params['prependTag'] . '(.*?)' . Yii::$app->params['appendTag'] . '@si', $render, $array);
        $count_match = 0;
        foreach ($array[1] as $value) {
            $count_match += str_word_count(trim($value));
            $a .= $value;
        }

        $count_words = str_word_count(trim($string1));
        $result['text_diff']=$render;
        $result['similarity']=round($count_match / $count_words * 100, 2);

        return $result;
    }

    protected function plagiarismCheck($id) {
        $compare=  PlagiarismCompare::findOne($id);
            $model = Literature::findOne($compare->literature_id);
            
            $test = $this->compare($model->konten, $compare->content);
            $compare->text_diff = $test["text_diff"];
            $compare->similarity = $test["similarity"];
            $compare->save();
            $plagiarismCompares = PlagiarismCompare::find()->where(['plagiarism_test_id' => $compare->plagiarism_test_id, 'literature_id' => $compare->literature_id])->all();
            $hasil = 0;
            foreach ($plagiarismCompares as $plagiarismCompare) {
                //check result
                $value = \app\models\Params::findOne(['name' => 'Minimum Result'])->value;
                if ($plagiarismCompare->similarity >= $value) {
                    $hasil += 1;
                }
            }
            $plagiarismTest = PlagiarismTest::findOne($compare->plagiarism_test_id);
            $plagiarismTest->result = $hasil;
            $plagiarismTest->save();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'id' => (string)$compare->_id,
                'result' => $compare->similarity,
            ];
    }

    public function actionStart() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $hasil = '';
            PlagiarismCompare::deleteAll(['literature_id' => $id, 'plagiarism_test_id' => '']);
            $test = new PlagiarismTest();
            $test->document_id = $id;
            $test->type = 'Literature';
            $test->submit_by = Yii::$app->user->identity->id;
            $test->datetime = date('Y-m-d H:i:s');
            $test->result = 0;
            $test->save();
            $model= Literature::findOne($test->document_id);
            $model->plagiarism_check_status="Processing";
            $model->save(false);
            //search kandidat
            $candidates=$this->Submission($id,(string)$test->_id);
            $model->plagiarism_check_status="Tested";
            $model->save(false);
            return $this->renderAjax('result', [
                'model' => $model,
                'test' => $test,
            ]);
            // return $this->renderAjax('_filter', [
            //             'results' => PlagiarismCompare::find()->where(['literature_id' => $id,'plagiarism_test_id'=>(string)$test->_id])->orderBy(['source'=>SORT_ASC,'url'=>SORT_ASC])->all(),
            // ]);
        }
    }

    protected function Submission($id,$test_id) {
        $limit = \app\models\Params::findOne(['name' => 'Limit Search'])->value;
        $model = Literature::findOne($id);
        $query = new Query();
        if((int)$limit==0){
        $query->select(['judul', 'konten', 'author_id', 'score' => ['$meta' => 'textScore']])
                ->from('literature')
                ->where(['$text' => ['$search' => str_replace(',',' ',$model->keyword)]])
                ->andWhere(['<>', '_id', $model->_id])
                ->andWhere(['=', 'pembanding', 'Y'])
                ->andWhere(['=', 'jenis_kegiatan', $model->jenis_kegiatan])
                ->orderBy(['score' => ['$meta' => 'textScore']]);
        }else{
            $query->select(['judul', 'konten', 'author_id', 'score' => ['$meta' => 'textScore']])
                ->from('literature')
                ->where(['$text' => ['$search' => str_replace(',',' ',$model->keyword)]])
                ->andWhere(['<>', '_id', $model->_id])
                ->andWhere(['=', 'pembanding', 'Y'])
                ->andWhere(['=', 'jenis_kegiatan', $model->jenis_kegiatan])
                ->orderBy(['score' => ['$meta' => 'textScore']])
                ->limit($limit);
        }
        $querys = $query->all();
        $hasil = FALSE;
        if ($querys) {
            foreach ($querys as $query) {
                $user= \app\models\User::findOne($query['author_id'])->name;
                $compare = new PlagiarismCompare();
                $compare->source = 'Submission';
                $compare->title = $query['judul'];
                $compare->plagiarism_test_id = $test_id;
                $compare->content = $query['konten'];
                $compare->author = $user;
                $compare->literature_id = $id;
                $compare->snippet = $query['judul'];
                $compare->url = 'FROM SUBMISSION';
                $compare->year = date('Y');
                $compare->save();
                //check similarity
                $this->plagiarismCheck((string)$compare->_id);
                $hasil = TRUE;
            }
        }
        return $hasil;
    }
    
    public function actionGetDocument() {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $model=  \app\models\Literature::findOne($id);
            return $this->renderAjax('/literature/_view', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUploadManual($id)
    {
       $model=  \app\models\PlagiarismCompare::findOne($id);
        
        return $this->renderAjax('_manual', [
            'model' => $model,
        ]);
    }
    
    /**
     * Displays a single PlagiarismTest model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PlagiarismTest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id=NULL)
    {
        $model = new PlagiarismTest();
        if($id <> NULL){
            $model->document_id=$id;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'id' => $id,
            ]);
        }
    }

    /**
     * Updates an existing PlagiarismTest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PlagiarismTest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
     public function actionCertificate($id) {
        $model=$this->findModel($id);
        return $this->render('certificate', [
                    'model' => $model,
                    'literature' => \app\models\Literature::findOne($model->document_id),
        ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PlagiarismTest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return PlagiarismTest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PlagiarismTest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
