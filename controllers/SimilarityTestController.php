<?php

namespace app\controllers;

use Yii;
use app\models\SimilarityTest;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Asset;
use yii\filters\AccessControl;

class SimilarityTestController extends Controller {
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionCreate() {
        $model = new SimilarityTest;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->method == 'File') {
                $model->file1 = UploadedFile::getInstance($model, 'file1');
                $model->file2 = UploadedFile::getInstance($model, 'file2');

//                if ($model->file1 && $model->file2 && $model->validate()) {
                    $model->file1->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $model->file1);
                    $model->file2->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $model->file2);
//                }
            }
            
            if ($model->save(false)) {
                if ($model->method == 'File') {
                    $filename1 = Yii::getAlias('@webroot') . '/uploads/' . $model->file1;
                    $asset1 = new Asset();
                    $asset1->adhoc_id = (string) $model->_id.'1';
                    $asset1->literature_id = '';
                    $asset1->file = $filename1;
                    $asset1->contentType = $model->file1->type;
                    $asset1->filename = $model->file1->name;
                    $asset1->uploadDate = date('Y-m-d h:i:s');
                    $asset1->save();
                    
                    $filename2 = Yii::getAlias('@webroot') . '/uploads/' . $model->file2;
                    $asset2 = new Asset();
                    $asset2->adhoc_id = (string) $model->_id.'2';
                    $asset2->literature_id = '';
                    $asset2->file = $filename2;
                    $asset2->contentType = $model->file2->type;
                    $asset2->filename = $model->file2->name;
                    $asset2->uploadDate = date('Y-m-d h:i:s');
                    $asset2->save();
                }
                return $this->redirect(['home', 'id' => (string)$model->_id]);
            }
        }
    }
}