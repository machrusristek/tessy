<?php

namespace app\controllers;

use Yii;
use app\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \app\models\User;
use yii\web\NotFoundHttpException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use devmustafa\blog\models\Post;
use app\models\SimilarityTest;
use app\models\Asset;


/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => '\yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function successCallback($client) {
        $attributes = $client->getUserAttributes();
        // user login or signup comes here
//        var_dump($attributes);exit();
        $user = User::findByUsername(strtolower(str_replace(' ', '', $attributes['email'])));
        if ($user == NULL) {
            $security = new \yii\base\Security();
            $user = new User();
//                    $user->id = NULL;
//            $user->username = $this->username;
            $user->name = strtolower(str_replace(' ', '', $attributes['name']));
            $user->setPassword(md5($security->generateRandomString()));
//            $user->email = $this->username . '@mail.ugm.ac.id';
            $user->email = $attributes['email'];
            $user->auth_mode = 'FACEBOOK';
            $user->username = $attributes['email'];
            $user->password_reset_token = null;
            $user->generateAuthKey();
//            $user->email_confirmed = true;
//            $user->ldap_user = true;
            $user->save();
//            User::assignRole($user->id, 'Supervisor');
        }
//        return Yii::$app->user->login($user, $this->rememberMe ? Yii::$app->user->cookieLifetime : 0);
        return Yii::$app->user->login($user);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
//        $client = new \Google_Client();
//$client->setApplicationName("Client_Library_Examples");
//$client->setDeveloperKey("AIzaSyDx4BH7bcF8DKDr8YfAcy_Ji01nFO2jpjU");
// $service = new \Google_Service_Customsearch($client);
//  $arrOptions = array();
//  $arrOptions['cx'] = '017167330966179737393:8iktuphpare'; // masukkan Search Engine ID
//  $q = 'Paris Hilton'; // contoh keyword yang ingin dicari
//  $result = $service->cse->listCse($q,$arrOptions);
//  if (isset($result['items'])) {
//    // hasil pencarian ada di index "items"
//    foreach($result['items'] AS $i => $resultItem) {
//      // tampilkan beberapa informasi yang bermanfaat
//      $hasil[]= "
//        <p>
//        No : " .$i. "<br />
//        Title : ".$resultItem['title']."<br />
//        HTML Title : ".$resultItem['htmlTitle']."<br />
//        Display Link : ".$resultItem['displayLink']."<br />
//        Formatted Link : ".$resultItem['formattedUrl']."<br />
//        Snippet : ".$resultItem['snippet']."<br />
//        HTML Snippet : ".$resultItem['htmlSnippet']."<br />
//        </p>
//      ";
//    }
//  }
//var_dump($hasil);exit();
        if (\Yii::$app->user->isGuest) {
            return $this->redirect('login');
        } else {
            if(\Yii::$app->user->identity->role == '' && \Yii::$app->user->identity->university == ''){
                return $this->redirect(['update-profile']);
            }else{
                return $this->redirect(['literature/index']);
                //return $this->redirect(['site/index']);
            }
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin($alert=false) {

        if (!\Yii::$app->user->isGuest) {
            //$this->goBack();
            return $this->redirect(['literature/index']);
        }

        $model = new LoginForm();
        $model2 = new SignupForm();
        if ($model->load($_POST) && $model->login()) {
            if (Yii::$app->user->identity->role == '' && Yii::$app->user->identity->university == '') {
                return $this->redirect(['update-profile']);
            }else if(Yii::$app->user->identity->role == 'adminUniversitas'){
                return $this->redirect(['site/dashboard']);
            }else {
                return $this->redirect(['literature/index']);
            }
        } else {
            return $this->render('login', [
                        'model' => $model,
                        'model2' => $model2,
                        'alert' => $alert
            ]);
        }

        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                if (Yii::$app->user->identity->role == '' && Yii::$app->user->identity->university == '') {
                    return $this->redirect(['update-profile']);
                } else {
                    return $this->goBack();
                }
            } /*else {
                return $this->redirect('/home');
            }*/
        /*}
        return $this->renderAjax('login', [
                        'model' => $model,
        ]);*/
    }

    public function actionLoginHome() {

        if (!\Yii::$app->user->isGuest) {
            $this->goBack();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
          if ($model->login()) {
            if (Yii::$app->user->identity->role == '' && Yii::$app->user->identity->university == '') {
                return $this->redirect(['update-profile']);
            } else if(Yii::$app->user->identity->role == 'adminUniversitas'){
                return $this->redirect(['site/dashboard']);
            }else {
                return $this->redirect(['literature/index']);
            }
          }else{
            \Yii::$app->session->setFlash('error', 'Wrong username and password');
            return $this->redirect(['/login']);
          }
        }

        return $this->renderAjax('login-home', [
                    'model' => $model,
        ]);
        /*
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                if (Yii::$app->user->identity->role == '' && Yii::$app->user->identity->university == '') {
                    return $this->redirect(['update-profile']);
                } else {
                    return $this->goBack();
                }
            } /*else {
                return $this->redirect('/home');
            }*/
        /*}
        return $this->renderAjax('login', [
                        'model' => $model,
        ]);*/
    }

     public function actionVerification($u = NULL, $p = NULL) {
        if ($u <> NULL && $p <> NULL) {
            $model = new LoginForm();
            $model->username = $u;
            $model->password = $p;
            if ($model->login()) {
                return $this->goHome();
            } else {
                return $this->goBack();
            }
        } else {
            return $this->goBack();
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout() {
        return $this->render('about');
    }


     public function actionDaftar() {
        return $this->render('daftar');
    }

    public function actionUpdateProfile() {
        $model = $this->findModel((string)Yii::$app->user->identity->id);
        $model->scenario = 'update';
        if ($model->load(Yii::$app->request->post())) {
            $model->username =  $model->email;
            $model->image = UploadedFile::getInstance($model, 'image');
            if($model->save()){
                if ($model->image) {
              // Working directory
                $dir = Yii::getAlias('@webroot') . '/uploads/profile/'.(string)$model->_id;
                FileHelper::createDirectory($dir);
                // Save main image
                $model->image->saveAs($dir . '/main.jpg');
                }
                Yii::$app->session->setFlash('info', Yii::t('app', 'Your profile details have been updated'));
                return $this->redirect(['update-profile']);
            }
        }else {

        return $this->render('update_profile',[
            'model' => $model
        ]);
        }
    }

    public function actionAccount()
    {
        /** @var SettingsForm $model */
        $model = Yii::createObject(\app\models\SettingsForm::className());
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->session->setFlash('info', Yii::t('app', 'Your account details have been updated'));
                return $this->redirect(['account']);
            }

        }

        return $this->render('account', [
            'model' => $model,
        ]);
    }

        /**
     * Signs user up.
     *
     * @return mixed
     */
    protected function genRndString($length = 8, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        if ($length > 0) {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars)) {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1})
                    $the_chars .= $r;
            }

            return $the_chars;
        }
    }

    public function actionRegister()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->username=$model->email;
            $password = $this->genRndString();
            $model->password = $password;
            $model->role = 'Applicant';

            if ($model->validate()) {
            if ($user = $model->signup()) {
                $mailer = Yii::$app->mailer;
                $params = ['module' => $this->module,'password'=>$password, 'model' => $user];
                $mailer->compose('@app/views/site/_email', $params)
                ->setTo($model->email)
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setSubject(Yii::t('app', 'Tessy Registration'))
                ->send();
                $model = new SignupForm();
                return $this->render('registration', [
                    'model' => $model,
                    'alert'=>TRUE
                ]);
            }
            }
        }

        return $this->render('registration', [
            'model' => $model,
            'alert'=>FALSE
        ]);
    }


    public function actionHome() {
        if(!\Yii::$app->user->isGuest) {
            return $this->redirect('literature/index');
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
          if ($model->login()) {
            if (Yii::$app->user->identity->role == '' && Yii::$app->user->identity->university == '') {
                return $this->redirect(['update-profile']);
            } else if(Yii::$app->user->identity->role == 'adminUniversitas'){
                return $this->redirect(['site/dashboard']);
            }else {
                return $this->redirect(['literature/index']);
            }
          }else{
            \Yii::$app->session->setFlash('error', 'Wrong username and password');
            return $this->redirect(['/login']);
          }
        }

        return $this->render('home', [
                    'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionValidate($id) {
        $model= \app\models\PlagiarismTest::findOne($id);
        if($model){
        $literature= \app\models\Literature::findOne($model->document_id);
        }else{
         $literature=NULL;
        }
        return $this->render('validate', [
                    'model' =>$model,
                    'literature' =>$literature,
        ]);
    }

    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * [pdftotext description]
     * @param  [type] $file [description]
     * @return [type]       [description]
     */
    function pdftotext($file) {
        try {
            $content = shell_exec(Yii::getAlias('@bin') . Yii::$app->params['pdfBin'] . '/pdftotext -nopgbrk -q -raw -l ' . Yii::$app->params['extractMaxPage'] . ' "' . $file . '" -');
            return utf8_encode($content);
        } catch (Exception $e) {
            return 'ERROR:' . $e->getMessage();
        }
    }

    public function actionSimilarityTest() {
        $model = new SimilarityTest;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->method == 'File') {
                $model->file1 = UploadedFile::getInstance($model, 'file1');
                $model->file2 = UploadedFile::getInstance($model, 'file2');

//                if ($model->file1 && $model->file2 && $model->validate()) {
                    $model->file1->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $model->file1);
                    $model->file2->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $model->file2);
//                }
            }
            
            if ($model->save(false)) {
                if ($model->method == 'File') {
                    $filename1 = Yii::getAlias('@webroot') . '/uploads/' . $model->file1;
                    $asset1 = new Asset();
                    $asset1->adhoc_id = (string) $model->_id.'1';
                    $asset1->literature_id = '';
                    $asset1->file = $filename1;
                    $asset1->contentType = $model->file1->type;
                    $asset1->filename = $model->file1->name;
                    $asset1->uploadDate = date('Y-m-d h:i:s');
                    $asset1->save();
                    
                    $filename2 = Yii::getAlias('@webroot') . '/uploads/' . $model->file2;
                    $asset2 = new Asset();
                    $asset2->adhoc_id = (string) $model->_id.'2';
                    $asset2->literature_id = '';
                    $asset2->file = $filename2;
                    $asset2->contentType = $model->file2->type;
                    $asset2->filename = $model->file2->name;
                    $asset2->uploadDate = date('Y-m-d h:i:s');
                    $asset2->save();
                }
                return $this->redirect(['similarity-test-view', 'id' => (string)$model->_id]);
                
            }
            //$similarity = SimilarityTest::find()->where(['_id' => (string)$model->_id])->asArray()->one();
            //echo json_encode($similarity);
        }
    }

    public function actionSimilarityTestView($id) {
        $model = SimilarityTest::findOne($id);
        $queri = (new Post)->getPostsList('', 4, '');
        $news = $queri->orderBy(['publish_date'=>SORT_DESC])->all();
        return $this->render('similarity-test-view', [
                    'model' => $model,
                    'news' => $news
        ]);
        
    }

    public function actionDownload()
    {
        return \Yii::$app->response->sendFile(Yii::getAlias('@webroot') . '/uploads/Manual.of.Tesy.3.0.pdf', 'Manual of Tessy 3.0', ['inline'=>true]);
    }

    public function actionDashboard()
    {
        return $this->render('dashboard');
    }

}
