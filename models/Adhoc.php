<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "adhoc".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property string $author1
 * @property string $year1
 * @property string $text1
 * @property string $author2
 * @property string $year2
 * @property string $text2
 * @property double $similarity
 * @property string $diff
 * @property string $method
 * @property integer $user_id
 * @property string $create_time
 * @property string $update_time
 */
class Adhoc extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file1;
    public $file2;
    public static function collectionName()
    {
        return 'adhoc';
    }
    
     public function attributes()
    {
        return [
            '_id',
            'author1',
            'year1',
            'text1',
            'author2',
            'year2',
            'text2',
            'similarity',
            'diff',
            'method',
            'user_id',
            'create_time',
            'update_time'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author1', 'year1', 'author2', 'year2', 'method'], 'required'],
            [['year1', 'year2', 'create_time', 'update_time'], 'safe'],
            [['text1', 'text2',  'diff', 'method'], 'string'],
            [['similarity'], 'number'],
            [['user_id'], 'integer'],
            [['author1', 'author2'], 'string', 'max' => 100],
            [['file1', 'file2'], 'file', 'extensions' => 'pdf', 'maxSize' => 25000 * 1024, 'tooBig' => 'Limit is 25 MB','when'=>function ($attribute) {
                return $this->method == 'File';
                    }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'author1' => Yii::t('app', 'Testee Author'),
            'year1' => Yii::t('app', 'Testee Year'),
            'file1' => Yii::t('app', 'Testee File'),
            'text1' => Yii::t('app', 'Testee Text'),
            'author2' => Yii::t('app', 'Tester Author'),
            'year2' => Yii::t('app', 'Tester Year'),
            'file2' => Yii::t('app', 'Tester File'),
            'text2' => Yii::t('app', 'Tester Text'),
            'similarity' => Yii::t('app', 'Similarity'),
            'diff' => Yii::t('app', 'Diff'),
            'method' => Yii::t('app', 'Method'),
            'user_id' => Yii::t('app', 'User ID'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
        ];
    }
    
    function pdftotext($file) {
        try {
            $content = shell_exec(Yii::getAlias('@bin') . Yii::$app->params['pdfBin'] . '/pdftotext -nopgbrk -q -layout "' . $file . '" -');
            return utf8_encode($content);
        } catch (Exception $e) {
            return 'ERROR:' . $e->getMessage();
        }
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        if ($insert) {
            if ($this->method == 'File') {
                
                if ($this->file1 != null && $this->file2 != null) {
////                    try {
//                        // Extracting first document
                        $filename1 = Yii::getAlias('@webroot') . '/uploads/' ."1-". $this->file1;
                        $content1 = $this->pdftotext($filename1);
                        if ($content1 == '' || substr($content1, 0, 5) == 'ERROR') {
                            Yii::$app->getSession()->setFlash('danger', "Konten File 1 Kosong");//'<strong>Oopss, something went wrong when extracting testee document, please see help page for further information!</strong>');
                            // throw new \yii\web\HttpException(405);
                        } else {
                            $this->text1 = $content1;
                        }
                        //                        unlink($filename1);
                        
                        // Extracting second document
                        $filename2 = Yii::getAlias('@webroot') . '/uploads/' ."2-". $this->file2;
                        $content2 = $this->pdftotext($filename2);
                        if ($content2 == '' || substr($content2, 0, 5) == 'ERROR') {
                            Yii::$app->getSession()->setFlash('danger', "Konten File 2 Kosong");//'<strong>Oopss, something went wrong when extracting testee document, please see help page for further information!</strong>');
                            // Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting tester document, please see help page for further information!</strong>');
                            // throw new \yii\web\HttpException(405);
                        } else {
                            $this->text2 = $content2;
                        }
                       
//                        unlink($filename2);
//                    } catch (\Exception $exc) {
//                        Yii::$app->getSession()->setFlash('danger', '<strong>Oopss, something went wrong when extracting your document, please see help page for further information!</strong>');
//                        throw new \yii\web\HttpException(405);
//                        return false;
//                    }
                }
            }
            
            $granularity = new \cogpowered\FineDiff\Granularity\Word;
            $renderer = new \cogpowered\FineDiff\Render\Html;
            $diff = new \cogpowered\FineDiff\Diff($granularity, $renderer);
            $str1 = $this->text1;
            $str2 = $this->text2;
            $render = $diff->render($str1, $str2);
            preg_match_all('@'.Yii::$app->params['prependTag'].'(.*?)'.Yii::$app->params['appendTag'].'@si', $render, $array);
            $count_match = 0;
            $a='';
            foreach ($array[1] as $value) {
                $count_match += str_word_count(trim($value));
                $a .= $value;
            }
            $count_words = str_word_count(trim($str1));
            $this->diff = $render;
            $this->similarity = round($count_match / $count_words * 100, 2);
            $this->create_time = new \yii\db\Expression('NOW()');
            $this->user_id = Yii::$app->user->id;
//            $this->author_name = $this->author->profile->name;
        } else {
            $this->update_time = new \yii\db\Expression('NOW()');
        }


        return true;
    }

}
