<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Adhoc;

/**
 * AdhocSearch represents the model behind the search form about `app\models\Adhoc`.
 */
class AdhocSearch extends Adhoc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['similarity','_id', 'user_id','author1', 'year1', 'text1', 'author2', 'year2',  'text2', 'diff', 'method', 'create_time', 'update_time'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adhoc::find();

        // add conditions that should always apply here
        
//        $query->andWhere('user_id = ' . Yii::$app->user->id);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'year1' => $this->year1,
            'year2' => $this->year2,
            'similarity' => $this->similarity,
            'create_time' => $this->create_time,
            'update_time' => $this->update_time,
        ]);

        $query->andFilterWhere(['like', 'author1', $this->author1])
            ->andFilterWhere(['like', 'text1', $this->text1])
            ->andFilterWhere(['like', 'author2', $this->author2])
            ->andFilterWhere(['like', 'text2', $this->text2])
            ->andFilterWhere(['like', 'diff', $this->diff])
            ->andFilterWhere(['like', 'method', $this->method]);

        if(Yii::$app->user->identity->role <> 'Superadmin'){
           $query->andFilterWhere(['=', 'user_id', Yii::$app->user->identity->id]);
        }
        
        return $dataProvider;
    }
}
