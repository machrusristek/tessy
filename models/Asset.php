<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "asset".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property array $filename
 * @property string $uploadDate
 * @property string $length
 * @property string $chunkSize
 * @property string $md5
 * @property array $file
 * @property string $newFileContent
 * Must be application/pdf, image/png, image/gif etc...
 * @property string $contentType
 * @property string $description
 * @property string $literature_id
 * @property string $adhoc_id
 */
class Asset extends \yii\mongodb\file\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'asset';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'filename',
            'uploadDate',
            'length',
            'chunkSize',
            'md5',
            'file',
            'newFileContent',
            'contentType',
            'description',
            'literature_id',
            'adhoc_id',
            'class_id',
            'user_id'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'uploadDate', 'length', 'chunkSize', 'md5', 'file', 'newFileContent', 'contentType', 'description', 'literature_id','adhoc_id', 'class_id', 'user_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'filename' => Yii::t('app', 'Filename'),
            'uploadDate' => Yii::t('app', 'Upload Date'),
            'length' => Yii::t('app', 'Length'),
            'chunkSize' => Yii::t('app', 'Chunk Size'),
            'md5' => Yii::t('app', 'Md5'),
            'file' => Yii::t('app', 'File'),
            'newFileContent' => Yii::t('app', 'New File Content'),
            'contentType' => Yii::t('app', 'Content Type'),
            'description' => Yii::t('app', 'Description'),
            'literature_id' => Yii::t('app', 'Literature'),
            'adhoc_id' => Yii::t('app', 'Adhoc'),
        ];
    }
    
    //  public function afterSave($insert, $changedAttributes) {
    //     parent::afterSave($insert, $changedAttributes);
    //     return true;
    // }
}
