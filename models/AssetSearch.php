<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Asset;

/**
 * AssetSearch represents the model behind the search form about `app\models\Asset`.
 */
class AssetSearch extends Asset
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'filename', 'uploadDate', 'length', 'chunkSize', 'md5', 'file', 'newFileContent', 'contentType', 'description', 'literature_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Asset::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'uploadDate', $this->uploadDate])
            ->andFilterWhere(['like', 'length', $this->length])
            ->andFilterWhere(['like', 'chunkSize', $this->chunkSize])
            ->andFilterWhere(['like', 'md5', $this->md5])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'newFileContent', $this->newFileContent])
            ->andFilterWhere(['like', 'contentType', $this->contentType])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'literature_id', $this->literature_id]);

        return $dataProvider;
    }
}
