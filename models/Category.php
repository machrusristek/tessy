<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "category".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $cluster
 * @property mixed $name
 */
class Category extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'cluster',
            'name',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cluster', 'name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'cluster' => Yii::t('app', 'Cluster'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
     public static function arraySelect()
    {
        $categorys=Category::find()->select(['_id', 'name'])->orderBy('name')->all();
        $kategori=array();
        foreach($categorys as $category){
          $kategori[(string) $category->_id]= $category->name;    
        }
        return $kategori;
     }
}
