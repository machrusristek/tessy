<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for collection "group".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $supervisor_id
 * @property mixed $name
 * @property mixed $reserved
 * @property mixed $description
 * @property mixed $created_time
 * @property mixed $plagiarism_treshold
 * @property mixed $published_time
 * @property mixed $deadline
 * @property mixed $slug
 */
class Group extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'supervisor_id',
            'name',
            'reserved',
            'description',
            'created_time',
            'plagiarism_treshold',
            'published_time',
            'deadline',
            'slug',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supervisor_id', 'name', 'reserved', 'description', 'created_time', 'plagiarism_treshold', 'published_time', 'deadline', 'slug'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'supervisor_id' => 'Supervisor ID',
            'name' => 'Name',
            'reserved' => 'Reserved',
            'description' => 'Description',
            'created_time' => 'Created Time',
            'plagiarism_treshold' => 'Plagiarism Treshold',
            'published_time' => 'Published Time',
            'deadline' => 'Deadline',
            'slug' => 'Slug',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }
}
