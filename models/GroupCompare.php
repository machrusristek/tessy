<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for collection "group_compare".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $group_id
 * @property mixed $tester
 * @property mixed $testee
 * @property mixed $diff
 * @property mixed $score
 */
class GroupCompare extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'group_compare';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'group_id',
            'tester',
            'testee',
            'diff',
            'score',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'tester', 'testee', 'diff', 'score'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'group_id' => 'Group ID',
            'tester' => 'Tester',
            'testee' => 'Testee',
            'diff' => 'Diff',
            'score' => 'Score',
        ];
    }

    /**
     * get group name
     * @return [string] [description]
     */
    public function getGroup()
    {
        return Group::findOne($this->group_id)->name;
    }

    /**
     * get tester name
     * @return string [description]
     */
    public function getTester()
    {
        return User::findOne($this->tester)->name;
    }

    /**
     * get testee name
     * @return string [description]
     */
    public function getTestee()
    {
        return User::findOne($this->testee)->name;
    }

    /**
     * get treshold percentage
     *
     * @return void
     */
    public function getTreshold()
    {
        return Group::findOne($this->group_id)->plagiarism_treshold;
    }
}
