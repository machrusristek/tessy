<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GroupCompare;

/**
 * GroupCompareSearch represents the model behind the search form of `app\models\GroupCompare`.
 */
class GroupCompareSearch extends GroupCompare
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'group_id', 'tester', 'testee', 'diff', 'score'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = GroupCompare::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->where(['group_id' => $id]);
        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'group_id', $this->group_id])
            ->andFilterWhere(['like', 'tester', $this->tester])
            ->andFilterWhere(['like', 'testee', $this->testee])
            ->andFilterWhere(['like', 'diff', $this->diff])
            ->andFilterWhere(['like', 'score', $this->score]);

        return $dataProvider;
    }
}
