<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for collection "group_member".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $class_id
 * @property mixed $student_id
 * @property mixed $document
 * @property mixed $created_time
 */
class GroupMember extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'group_member';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'class_id',
            'student_id',
            'document',
            'created_time',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id', 'student_id', 'document', 'created_time'], 'safe'],
            ['document', 'file', 'extensions' => 'pdf', 'maxSize' => 5000 * 1024, 'tooBig' => 'Limit is 5 MB'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'class_id' => 'Class ID',
            'student_id' => 'Applicant',
            'document' => 'Document',
            'created_time' => 'Created Time',
        ];
    }

    /**
     * [getStudent description]
     * @return [type] [description]
     */
    public function getStudent()
    {
        return User::findOne($this->student_id);
    }

    /**
     * [getSlug description]
     * @return [type] [description]
     */
    public function getSlug(){
        return Group::findOne($this->class_id)->slug;
    }

    /**
     * get class id
     * @return string sting id class
     */
    public function getClass()
    {
        return Group::findOne($this->class_id)->_id;
    }
}
