<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "keyword".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $nama
 * @property mixed $aktif
 */
class KategoriSbk extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'kategori_sbk';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'nama',
            'kode_status_aktif',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'kode_status_aktif'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'kode_status_aktif' => Yii::t('app', 'Kode Status Aktif'),
        ];
    }
    
    public static function arraySelect()
    {
        $categorys=  KategoriSbk::find()->select(['_id', 'nama'])->where(['kode_status_aktif'=>"1"])->orderBy('nama')->all();
        $kategori=array();
        foreach($categorys as $category){
          $kategori[$category->nama]= $category->nama;    
        }
        return $kategori;
     }
}
