<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "keyword".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $frequency
 */
class Keyword extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'keyword';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'frequency',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'frequency'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'frequency' => Yii::t('app', 'Frequency'),
        ];
    }
    
    public static function suggestKeywords($keyword, $limit = 20) {
        $keyword = static::find()->where(
                                array('like', 'name', '%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%')
                        )
                        ->limit($limit)->orderBy('frequency DESC, Name')->all();
        $names = array();
        foreach ($keyword as $keyword)
            $names[] = $keyword->name;
        return $names;
    }

    public static function string2array($keyword) {
        return preg_split('/\s*,\s*/', trim($keyword), -1, PREG_SPLIT_NO_EMPTY);
    }

    public static function array2string($keyword) {
        return implode(', ', $keyword);
    }

    public static function updateFrequency($oldKeywords, $newKeywords) {
        
        $oldKeywords = self::string2array($oldKeywords);
        $newKeywords = self::string2array($newKeywords);
        self::addKeywords(array_values(array_diff($newKeywords, $oldKeywords)));
        self::removeKeywords(array_values(array_diff($oldKeywords, $newKeywords)));
    }

    public static function addKeywords($keyword) {

        if (count($keyword) > 0) {
            $inKeywords = preg_replace('/(\S+)/i', '\'\1\'', $keyword);
           
//            $sql = "UPDATE keyword SET frequency=frequency+1 WHERE name IN (:joinKeywords)";
//            Yii::$app->mongodb->createCommand($sql)->bindValue(':joinKeywords', join(",", $inKeywords))->execute();

            foreach ($keyword as $name) {
                $model = static::findOne(['name'=>$name]);
                if ($model === null) {
                    $keyword = new Keyword();
                    $keyword->name = $name;
                    $keyword->frequency = 1;
                    $keyword->save();
                }else{
                   $model->frequency += 1;
                   $model->save(); 
                }
            }
        }
    }

    public static function removeKeywords($keyword) {
        if (empty($keyword))
            return;
        $inKeywords = preg_replace('/(\S+)/i', '\'\1\'', $keyword);
        foreach ($keyword as $name) {
                $model = static::findOne(['name'=>$name]);
                if ($model) {
                   if($model->frequency <= 0){
                     $model->delete();
                   }else{
                     $model->frequency -= 1;
                     $model->save();  
                   }
                }
            }
    }
}
