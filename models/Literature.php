<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "literature".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property string $title
 * @property string $keyword
 * @property string $language
 * @property integer $category_id
 * @property integer $year
 * @property string $abstract
 * @property integer $author_id
 * @property integer $academic_supervisor
 * @property integer $main_supervisor
 * @property integer $co_supervisor
 * @property string $type
 * @property string $summary
 * @property string $konten
 * @property string $citation
 * @property string $data_source
 * @property string $file
 * @property string $referral_id
 * @property datetime $create_time
 * @property datetime $update_time
 * @property string $status
 */
class Literature extends \yii\mongodb\ActiveRecord
{
    public $keywords_text;
    public $category;
    public $author;
    public $caption;
    public $test_method;
    public $keyword_tags;
    private $_oldKeyword;

    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'literature';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            "id_usulan" ,
            "author_id",
            "tahun_usulan",
            "id_kegiatan" ,
            "tahun_kegiatan",
            "judul" ,
            "pengarang",
            "file" ,
            "kategori_sbk" ,
            "kd_program_hibah",
            "bidang_fokus" ,
            "keyword",
            "abstrak" ,
            "konten" ,
            "pendahuluan",
            "latar_belakang",
            "tinjauan_pustaka",
            "tujuan",
            "metode" ,
            "status",
            "solusi_permasalahan",
            "gambaran_iptek" ,
            "daftar_pustaka" ,
            "data_source" ,
            "plagiarism_check_status",
            "plagiarism_check_time" ,
            "create_user",
            "create_time",
            "update_user",
            "update_time",
            "jenis_kegiatan",
            "plagiarism_result",
            "pembanding"
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'judul', 'tahun_usulan','tahun_kegiatan','keyword'           ,
            'pengarang' ,'abstrak' ,'kd_program_hibah'],'required'],
            [['id_usulan',
            'author_id',
            'id_kegiatan',
            'judul'     ,
            'tahun_usulan',
            'tahun_kegiatan',
            'keyword'           ,
            'pengarang'  ,
            'konten'  ,
            'abstrak'            ,
            'pendahuluan'        ,
            'latar_belakang'      ,
            'tinjauan_pustaka'    ,
            'tujuan'             ,
            'kategori_sbk' ,
            'kd_program_hibah',
            'bidang_fokus'     ,
            'metode'             ,
            'status',
            'solusi_permasalahan' ,
            'gambaran_iptek'      ,
            'daftar_pustaka','jenis_kegiatan',
            "data_source" ,
            "plagiarism_check_status",
            "plagiarism_check_time" ,
            "create_user",
            "create_time",
            "update_user",
            "update_time","plagiarism_result",'pembanding'], 'safe'],
            [['id_usulan', 'id_kegiatan'], 'unique', 'targetAttribute' => ['id_usulan', 'id_kegiatan'], 'message' => 'Kombinasi ID Usulan & ID Kegiatan sudah pernah di input'],
            ['file', 'required', 'on' => 'create'],
            ['file', 'file', 'extensions' => 'pdf', 'maxSize' => 50000 * 1024, 'tooBig' => 'Limit is 50 MB'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            "id_usulan" => 'ID Usulan',
            "tahun_usulan"=> 'Tahun usulan',
            "id_kegiatan" => 'ID Kegiatan',
            "tahun_kegiatan"=> 'Tahun kegiatan',
            "judul" => 'Judul',
            "pengarang"=> 'Pengarang',
            "file" => 'File',
            "kategori_sbk" => 'Kategori sbk',
            "kd_program_hibah"=> 'Kode program hibah',
            "bidang_fokus" => 'Bidang fokus',
            "keyword"=> 'Keyword',
            "abstrak" => 'Abstrak',
            "konten" => 'Konten',
            "pendahuluan"=> 'Pendahuluan',
            "latar_belakang"=> 'Latar belakang',
            "tinjauan_pustaka"=> 'Tinjauan pustaka',
            "tujuan"=> 'tujuan',
            "metode" => 'Metode',
            "solusi_permasalahan"=> 'Solusi permasalahan',
            "gambaran_iptek" => 'Gambaran iptek',
            "daftar_pustaka" => 'Daftar pustaka',
            "data_source" => 'Data source',
            "plagiarism_check_status"=> 'Status',
            "plagiarism_check_time" => 'Plagiarism check time',
            "create_user"=> 'Create user',
            "create_time"=> 'Create time',
            "update_user"=> 'Update user',
            "update_time"=> 'Update time',
        ];
    }


    public function afterFind() {
        $this->author= User::findOne($this->author_id);
        $keyword_tags = '';
        foreach (explode(',', $this->keyword) as $keyword) {
            $keyword_tags .= '<span class="label label-info">' . trim($keyword) . '</span> ';
        }
        $this->keyword_tags = $keyword_tags;
        $this->_oldKeyword = $this->keyword;
    }



    public function beforeSave($insert) {
        parent::beforeSave($insert);
        if ($insert) {
            $this->create_time = date('Y-m-d h:i:s');
        } else {
            $this->update_time = date('Y-m-d h:i:s');
        }


        return true;
    }

    /**
     * This is invoked after the record is deleted.
     */
    public function afterDelete() {
        parent::afterDelete();
        $s=  explode(',', $this->keyword);
        foreach($s as $d){
            $key = Keyword::findOne(['name'=>$d]);
            if($key){
            $key->frequency=$key->frequency -1;
            $key->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramHibah()
    {
        return ProgramHibah::findOne(['kode_program_hibah' =>  "$this->kd_program_hibah"]);
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        // Keyword::updateFrequency($this->keyword);
        
        return true;
    }
    
    public function getPlagiarismTest()
    {
        $data=PlagiarismTest::find()->where(['document_id' => (string)$this->_id])->orderBy("datetime desc")->one();
        return $data;
    }
}
