<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Literature;

/**
 * LiteratureSearch represents the model behind the search form about `app\models\Literature`.
 */
class LiteratureSearch extends Literature
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'id_usulan', 'tahun_usulan', 'id_kegiatan', 'tahun_kegiatan', 'judul', 'pengarang', 'file', 'kategori_sbk', 'kd_program_hibah', 'bidang_fokus', 'keyword', 'abstrak', 'konten', 'pendahuluan', 'latar_belakang', 'tinjauan_pustaka', 'tujuan', 'metode', 'solusi_permasalahan', 'gambaran_iptek', 'daftar_pustaka', 'data_source', 'plagiarism_check_status', 'plagiarism_check_time', 'create_user', 'create_time', 'update_user', 'update_time','jenis_kegiatan','plagiarism_result','pembanding'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Literature::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere(['like', '_id', $this->_id])
        $query->andFilterWhere(['=', 'jenis_kegiatan', $this->jenis_kegiatan])
        ->andFilterWhere(['=', 'tahun_kegiatan', $this->tahun_kegiatan])
        ->andFilterWhere(['like', 'judul', $this->judul])
        ->andFilterWhere(['like', 'pengarang', $this->pengarang])
        ->andFilterWhere(['=', 'plagiarism_result', $this->plagiarism_result])
        ->andFilterWhere(['=', 'pembanding', $this->pembanding])
        // ->andFilterWhere(['=', 'id_usulan', $this->id_usulan])
        // ->andFilterWhere(['=', 'tahun_usulan', $this->tahun_usulan])
        // ->andFilterWhere(['=', 'id_kegiatan', $this->id_kegiatan])
        // ->andFilterWhere(['like', 'file', $this->file])
        // ->andFilterWhere(['like', 'kategori_sbk', $this->kategori_sbk])
        // ->andFilterWhere(['like', 'kd_program_hibah', $this->kd_program_hibah])
        // ->andFilterWhere(['like', 'bidang_fokus', $this->bidang_fokus])
        // ->andFilterWhere(['like', 'keyword', $this->keyword])
        // ->andFilterWhere(['like', 'abstrak', $this->abstrak])
        // ->andFilterWhere(['like', 'konten', $this->konten])
        // ->andFilterWhere(['like', 'pendahuluan', $this->pendahuluan])
        // ->andFilterWhere(['like', 'latar_belakang', $this->latar_belakang])
        // ->andFilterWhere(['like', 'tinjauan_pustaka', $this->tinjauan_pustaka])
        // ->andFilterWhere(['like', 'tujuan', $this->tujuan])
        // ->andFilterWhere(['like', 'metode', $this->metode])
        // ->andFilterWhere(['like', 'solusi_permasalahan', $this->solusi_permasalahan])
        // ->andFilterWhere(['like', 'gambaran_iptek', $this->gambaran_iptek])
        // ->andFilterWhere(['like', 'daftar_pustaka', $this->daftar_pustaka])
        // ->andFilterWhere(['like', 'data_source', $this->data_source])
        ->andFilterWhere(['like', 'plagiarism_check_status', $this->plagiarism_check_status]);

        if(Yii::$app->user->identity->role == 'Applicant'){
           $query->andFilterWhere(['=', 'author_id', Yii::$app->user->identity->id]);
        }  
        if(Yii::$app->user->identity->role == 'Reviewer'){
            $query->andFilterWhere(['=', 'author_id', Yii::$app->user->identity->id]);
         }           
        
        return $dataProvider;
    }
}
