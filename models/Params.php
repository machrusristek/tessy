<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "params".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $value
 */
class Params extends \yii\mongodb\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function collectionName() {
        return 'params';
    }

    /**
     * @inheritdoc
     */
    public function attributes() {
        return [
            '_id',
            'name',
            'value',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['value', 'name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    public static function DateNews($time) {
     $time = strtotime(date('Y-m-d H:i:s')) - strtotime($time); // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
    }

}
