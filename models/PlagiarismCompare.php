<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "plagiarism_compare".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $source
 * @property mixed $url
 * @property mixed $formatedurl
 * @property mixed $title
 * @property mixed $content
 * @property mixed $literature_id
 * @property mixed $plagiarism_test_id
 * @property mixed $api_id
 * @property mixed $snippet
 * @property mixed $author
 * @property mixed $text_diff
 * @property mixed $similarity
 * @property mixed $year
 */
class PlagiarismCompare extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'plagiarism_compare';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'source',
            'title',
            'url',
            'formatedurl',
            'content',
            'literature_id',
            'api_id',
            'plagiarism_test_id',
            'snippet',
            'author',
            'text_diff',
            'similarity',
            'year',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['source','formatedurl', 'url','api_id', 'content','literature_id','title','snippet','author','text_diff','similarity','year','plagiarism_test_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'source' => Yii::t('app', 'Source'),
            'url' => Yii::t('app', 'URL'),
            'api_id' => Yii::t('app', 'API ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'Snippet' => Yii::t('app', 'snippet'),
            'literature_id' => Yii::t('app', 'Literature'),
            'text_diff' => Yii::t('app', 'Text diff'),
        ];
    }
}
