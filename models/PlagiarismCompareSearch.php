<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlagiarismCompare;

/**
 * PlagiarismCompareSearch represents the model behind the search form of `app\models\PlagiarismCompare`.
 */
class PlagiarismCompareSearch extends PlagiarismCompare
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'source', 'title', 'url', 'content', 'literature_id', 'result', 'snippet'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlagiarismCompare::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'literature_id', $this->literature_id])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'snippet', $this->snippet]);

        return $dataProvider;
    }
}
