<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "plagiarism_test".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $datetime
 * @property mixed $submit_by
 * @property mixed $document_id
 * @property mixed $type
 * @property mixed $result
 * @property mixed $result_note
 * 
 * @property Literature $literature
 */
class PlagiarismTest2 extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $submit;
    public $literature;
    public $test_method;
    public static function collectionName()
    {
        return ['tessy_len', 'plagiarism_test'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'datetime',
            'submit_by',
            'document_id',
            'type',
            'result',
            'result_note',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'submit_by', 'document_id', 'result','type', 'result_note'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'datetime' => Yii::t('app', 'Date Time'),
            'submit_by' => Yii::t('app', 'Submit By'),
            'document_id' => Yii::t('app', 'Document'),
            'result' => Yii::t('app', 'Result'),
            'result_note' => Yii::t('app', 'Result Note'),
        ];
    }
    
    
}
