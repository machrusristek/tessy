<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PlagiarismTest;

/**
 * PlagiarismTestSearch represents the model behind the search form about `app\models\PlagiarismTest`.
 */
class PlagiarismTestSearch extends PlagiarismTest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'datetime', 'submit_by', 'document_id', 'result', 'result_note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PlagiarismTest::find()->orderBy(['datetime'=>SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'datetime', $this->datetime])
            ->andFilterWhere(['like', 'document_id', $this->document_id])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'result_note', $this->result_note]);
            
        return $dataProvider;
    }
}
