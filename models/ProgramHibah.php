<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "keyword".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $nama
 * @property mixed $aktif
 */
class ProgramHibah extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'program_hibah';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'kode_program_hibah',
            'nama',
            'kode_status_aktif',
            'kode_jenis_kegiatan',
            'jenis_kegiatan',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama','kode_program_hibah',
            'kode_status_aktif',
            'kode_jenis_kegiatan',], 'required'],
            ['kode_program_hibah','unique'],
            [['nama','kode_program_hibah',
            'kode_status_aktif',
            'kode_jenis_kegiatan',
            'jenis_kegiatan',], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'kode_status_aktif' => Yii::t('app', 'Kode Status Aktif'),
            'kode_jenis_kegiatan'=> Yii::t('app', 'Kode Jenis Kegiatan'),
            'jenis_kegiatan'=> Yii::t('app', 'Jenis Kegiatan'),
        ];
    }

    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->jenis_kegiatan=$this->kode_jenis_kegiatan==1?"PENELITIAN":"PENGABDIAN";
        /*
        if ($insert) {
            $this->user_id = Yii::$app->user->id;
            $this->create_time = new \yii\db\Expression('NOW()');
        } else {
            $this->update_time = new \yii\db\Expression('NOW()');
        }
        */
        return true;
    }

    public static function arraySelect()
    {
        $categorys=  ProgramHibah::find()->select(['kode_program_hibah','nama'])->where(['kode_status_aktif'=>"1"])->orderBy('kode_program_hibah')->all();
        $kategori=array();
        foreach($categorys as $category){
          $kategori[$category->kode_program_hibah]= $category->nama;    
        }
        return $kategori;
     }
    
}
