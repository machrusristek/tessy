<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProgramHibah;

/**
 * ProgramHibahSearch represents the model behind the search form of `app\models\ProgramHibah`.
 */
class ProgramHibahSearch extends ProgramHibah
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['_id','kode_program_hibah', 'nama', 'kode_status_aktif', 'kode_jenis_kegiatan', 'jenis_kegiatan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProgramHibah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['=', 'kode_program_hibah', $this->kode_program_hibah])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'kode_status_aktif', $this->kode_status_aktif])
            ->andFilterWhere(['like', 'kode_jenis_kegiatan', $this->kode_jenis_kegiatan])
            ->andFilterWhere(['like', 'jenis_kegiatan', $this->jenis_kegiatan]);

        return $dataProvider;
    }
}
