<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "params".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $value
 */
class ProgramStudy extends \yii\mongodb\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function collectionName() {
        return 'program_study';
    }

    /**
     * @inheritdoc
     */
    public function attributes() {
        return [
            '_id',
            'name',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[ 'name'], 'required'],
            [[ 'name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public static function arraySelect()
    {
        $categorys=  ProgramStudy::find()->select(['_id', 'name'])->orderBy('name')->all();
        $kategori=array();
        foreach($categorys as $category){
          $kategori[$category->name]= $category->name;    
        }
        return $kategori;
     }

}
