<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * SettingsForm gets user's username, email and password and changes them.
 *
 * @property User $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SettingsForm extends Model
{

    /** @var string */
    public $new_password;

    /** @var string */
    public $current_password;
    
    public $password_repeat;

    /** @var User */
    private $_user;

    /** @return User */
    public function getUser()
    {
        if ($this->_user == null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }


    /** @inheritdoc */
    public function rules()
    {
        return [
            'newPasswordLength' => ['new_password', 'string', 'max' => 72, 'min' => 6],
            'currentPasswordRequired' => ['current_password', 'required'],
            'currentPasswordValidate' => ['current_password', function ($attr) {
                if (!$this->passwordValidate($this->$attr, $this->user->password_hash)) {
                    $this->addError($attr, Yii::t('user', 'Current password is not valid'));
                }
            }],
            'password_repeat' => ['password_repeat', 'compare', 'compareAttribute' => 'new_password', 'message'=>\Yii::t('app', 'Passwords don\'t match')],
            'password_repeatRequired' => ['password_repeat', 'required','when'=>function ($attribute) {
                    return $this->new_password != '';
                    }, 'whenClient' => "function (attribute, value) {
                                    return $('#settings-form-new_password').val() != '';
                    }"]
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'new_password'     => Yii::t('user', 'New password'),
            'current_password' => Yii::t('user', 'Current password'),
        ];
    }

    /** @inheritdoc */
    public function formName()
    {
        return 'settings-form';
    }
    
    public static function passwordValidate($password, $hash)
    {
        return Yii::$app->security->validatePassword($password, $hash);
    }

    /**
     * Saves new account settings.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
//            $this->user->scenario = 'settings';
            $this->user->setPassword($this->new_password);
            return $this->user->save();
        }

    }

}
