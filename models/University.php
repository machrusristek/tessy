<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "university".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 */
class University extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'university';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'url',
            'active',
            'premium'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name','url','active','premium'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'URL'),
            'active' => Yii::t('app', 'Actived URL'),
            'premium' => Yii::t('app', 'Premium University'),
        ];
    }
    
    public static function arraySelect()
    {
        $categorys=  University::find()->select(['_id', 'name'])->orderBy('name')->all();
        $kategori=array();
        foreach($categorys as $category){
          $kategori[$category->name]= $category->name;    
        }
        return $kategori;
     }
}
