<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\mongodb\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;

/**
 * User model
 *
 * @property object $_id
 * @property string $name
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $role
 * @property string $university
 * @property string $mobile_phone
 * @property string $gender
 * @property string $address
 * @property string $auth_key
 * @property string $auth_mode
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface {

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public $image;
    public static function collectionName() {
        return 'user';
    }

    public function attributes() {
        return [
            '_id',
            'name',
            'username',
            'password',
            'password_hash',
            'password_reset_token',
            'email',
            'role',
            'university',
            'mobile_phone',
            'program_study',
            'gender',
            'address',
            'auth_key',
            'auth_mode',
            'status',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
//            'attributes'=>[
//                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
//                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
//            ]
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'update'   => ['name','role','mobile_phone','gender','address','password'],
            // 'changePassword'   => ['password_hash'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique','on'=> 'create', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique','on'=> 'create', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
            
            [['name','mobile_phone','gender','address'], 'required', 'on' => ['update']],
            [['name','role','gender','address','auth_mode'], 'string'],
            [['mobile_phone'], 'number'],
            ['image', 'image','mimeTypes' => 'image/jpeg, image/png', 'extensions' => 'png, jpg', 'maxSize' => 1000 * 1024, 'tooBig' => 'Limit is 1 MB'],
            
//            ['password', 'required', 'on' => ['settings']],
//            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getMainImage()
    {
        return Yii::getAlias('@web') .Yii::$app->params['imageUrl'].'/uploads/profile/' .$this->id . '/main.jpg';
    }
    
    public static function findIdentity($id) {
        return static::findOne([
                    '_id' => $id,
                    'status' => self::STATUS_ACTIVE]
        );
    }


    public static function arrayTeacher()
    {
        $categorys=  User::find()->select(['_id', 'name'])->where(['=', 'role', 'Reviewer'])->all();

        $kategori=array();
        foreach($categorys as $category){
          $kategori[(string) $category->_id]= $category->name;
        }
        return $kategori;
     }

    /**
     * @inheritdoc
     */
    // public static function findIdentityByAccessToken($token, $type = null) {
    //     throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    // }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = strtotime('+6 hours');
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->getPrimaryKey()->__toString();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

}
