<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdhocSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adhoc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'author1') ?>

    <?= $form->field($model, 'year1') ?>

    <?= $form->field($model, 'file1') ?>

    <?= $form->field($model, 'text1') ?>

    <?php // echo $form->field($model, 'author2') ?>

    <?php // echo $form->field($model, 'year2') ?>

    <?php // echo $form->field($model, 'file2') ?>

    <?php // echo $form->field($model, 'text2') ?>

    <?php // echo $form->field($model, 'similarity') ?>

    <?php // echo $form->field($model, 'diff') ?>

    <?php // echo $form->field($model, 'method') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'update_time') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
