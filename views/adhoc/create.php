<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Adhoc */

$this->title = Yii::t('app', 'Adhoc Comparison');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Adhocs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adhoc-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
