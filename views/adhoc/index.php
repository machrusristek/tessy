<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdhocSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Adhoc Comparison');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adhoc-index">
    <div class="row">
        <div class="col-md-12">  

            <!-- About Me Box -->
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'author1',
                            'author2',
                            [
                                'attribute' => 'method',
                                'format' => 'html',
                                'filter' => ['File' => 'File', 'Text' => 'Text'],
                                'value' => function ($model, $key, $index, $widget) {
                            if ($model->method == 'File') {
                                return '<span class="label label-info">' . $model->method . '</span>';
                            } else {
                                return '<span class="label label-warning">' . $model->method . '</span>';
                            }
                        },
                            ],
                            [
                                'attribute' => 'similarity',
                                'format' => 'html',
                                'value' => function ($model, $key, $index, $widget) {
                                    return $model->similarity . '%';
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{view} {delete}',
                                'header' => 'Action',
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        $url = \yii\helpers\Url::toRoute(['view', 'id' => (string) $model->_id]);
                                        return Html::a('View Result', $url, [
                                                    'title' => Yii::t('yii', 'View Result'),
                                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                        ]);
                                    },
                                            'delete' => function ($url, $model) {
                                        $url = \yii\helpers\Url::toRoute(['delete', 'id' => (string) $model->_id]);
                                        return Html::a('Delete', $url, [
                                                    'title' => Yii::t('yii', 'Delete'),
                                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                                    'data-confirm' => 'Are you sure you want to delete this literature, all related data will be lost?',
                                                    'data-method' => 'POST'
                                        ]);
                                    },
                                        ],
                                    ],
                                ],
                            ]);
                            ?>
                </div>
            </div>
        </div>
    </div>
</div>