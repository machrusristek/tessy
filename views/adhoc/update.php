<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adhoc */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Adhoc',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Adhocs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="adhoc-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
