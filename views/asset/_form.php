<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Asset */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'filename') ?>

    <?= $form->field($model, 'uploadDate') ?>

    <?= $form->field($model, 'length') ?>

    <?= $form->field($model, 'chunkSize') ?>

    <?= $form->field($model, 'md5') ?>

    <?= $form->field($model, 'file') ?>

    <?= $form->field($model, 'newFileContent') ?>

    <?= $form->field($model, 'contentType') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'literatur_id') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
