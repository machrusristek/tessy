<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AssetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asset-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'filename') ?>

    <?= $form->field($model, 'uploadDate') ?>

    <?= $form->field($model, 'length') ?>

    <?= $form->field($model, 'chunkSize') ?>

    <?php // echo $form->field($model, 'md5') ?>

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'newFileContent') ?>

    <?php // echo $form->field($model, 'contentType') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'literatur_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
