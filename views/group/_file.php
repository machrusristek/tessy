<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Literature */

?>
<div class="literature-view">

    <?php if(strpos($asset->contentType,'image')===false): //Not an image?>
        <iframe src="<?=Url::to(['group/get', 'id'=>(string)$asset->class_id, 'user_id' => (string)$asset->user_id]);?>" width="100%" height="600px"></iframe>
    <?php else: ?>
        <?= Html::img(Url::to(['group/get', 'id'=>(string)$asset->class_id, 'user_id' => (string)$asset->user_id]), ['alt'=>$asset->description]);?>
    <?php endif; ?>

</div>
