<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'supervisor_id')->hiddenInput(['value' => \Yii::$app->user->identity->id])->label(false) ?>

    <?= $form->field($model, 'name') ?>

    <?php
        $model->reserved = false;
        echo $form->field($model, 'reserved')->widget(SwitchInput::className(),[
            'pluginOptions' => [
                'size' => 'medium',
                'onText' => 'Yes',
                'offText' => 'No',
            ]
        ]) 
    ?>

    <?= $form->field($model, 'description')->textArea() ?>
    
    <?= $form->field($model, 'created_time')->hiddenInput(['value'=> date('Y-m-d H:i:s')])->label(false) ?>

    <?= $form->field($model, 'plagiarism_treshold') ?>

    <?= $form->field($model, 'published_time')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter Published time ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <?= $form->field($model, 'deadline')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter Deadline time ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
