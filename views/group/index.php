<?php

namespace Users\rio\Sites\tessy\views\group;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Groups';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                'name',
                                'reserved',
                                'description',
                                // 'created_time',
                                'plagiarism_treshold',
                                'published_time',
                                'deadline',
                                // 'slug',

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view}{update}',
                                    'header' => 'Action',
                                    'buttons' => [
                                        'view' => function($url, $model){
                                            $url = Url::toRoute(['view', 'id' => (string)$model->_id]);
                                            return Html::a('View', $url,[
                                                'title' => Yii::t('yii', 'View'),
                                                'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                            ]);
                                        },
                                        'update' => function($url, $model){
                                            $url = Url::toRoute(['update', 'id' => (string)$model->_id]);
                                            return Html::a('Update', $url,[
                                                'title' => Yii::t('yii', 'Update'),
                                                'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                            ]);
                                        }
                                    ],
                                ],
                            ],
                        ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
