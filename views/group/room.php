<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

$this->title = 'Upload Document';
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['room']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-create">
	
	<div class="box box-primary">
		<div class="box-body">
			<strong><i class="fa fa-user margin-r-5"></i> Teacher</strong>
            <p class="text-muted">
				<?= $teacherName->name; ?>
            </p>
            <strong><i class="fa fa-bars margin-r-5"></i> Description</strong>
            <p class="text-muted">
				<?= $model->description; ?>
            </p>
            <strong><i class="fa fa-calendar margin-r-5"></i> Deadline</strong>
            <p class="text-muted">
				<?= date('d-m-Y H:i', strtotime($model->deadline)); ?>
            </p>
			<?php
                            $date_now = date('d-m-Y H:i');
                            $deadline = date('d-m-Y H:i', strtotime($model->deadline));
                            
                            if(strtotime($date_now) > strtotime($deadline)){
                                echo "<div class='callout callout-danger'>
                <h4>Deadline</h4>

                <p>Assignment for this class has deadline, please contact the teacher.</p>
              </div>";
                            }else{
                        ?>
			<?php
            $form = ActiveForm::begin([
                        'type' => ActiveForm::TYPE_HORIZONTAL,
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
            ]);
            ?>
            <?= $form->field($groupMember, 'document')->widget(kartik\widgets\FileInput::classname(), [
                'options' => ['accept' => 'application/pdf'],
                'pluginOptions' => [
                    'showRemove' => false,
                    'showUpload' => false,
                    
                ],
            ]); ?>

		</div>
		<div class="box-footer">
            <center>
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btn-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </center>
        </div>
        <?php ActiveForm::end(); ?>
                            <?php } ?>
	</div>
</div>