<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
<script type="text/javascript">
    var clipboard = new Clipboard('.btnGroupUrl');
    clipboard.on('success', function(e) {
        alert(`Url ${e.text} Copied!`);
        e.clearSelection();
    });
</script>
<?php

use app\models\Group;
use yii\bootstrap\Button;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $model Group */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="group-view">
    <div class="box box-primary">
        <div class="box-header with-border">
        </div>
        <div class="box-body">
            <?php Pjax::begin(); ?>
            <h3>Group Url : <a id="test" href="<?= Yii::$app->request->hostInfo."/group/".$model->slug."/".$model->deadline ?>" target="_blank"><?= Yii::$app->request->hostInfo."/group/".$model->slug."/".$model->deadline ?></a> <button class="btnGroupUrl" data-clipboard-target="#test">
                <i class="fa fa-clipboard" alt="Copy to clipboard"></i>
            </button></h3>
            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // '_id',
                    // 'id',
                    [
                        'attribute' => 'student_id',
                        'value' => function ($model) {
                            return $model->getStudent()->name;
                        }
                    ],
                    [
                        'attribute' => 'document',
                        'value' => function ($model){
                            return StringHelper::truncate($model->document, 300);
                        }
                    ],
                    [
                        'label'=>'File',
                        'format' => 'raw',
                        'value' => function($model){
                            return Html::a('Show file', ['group/file','id'=>(string)$model->getClass(), 'user_id' => (string)$model->getStudent()->_id], ['class' => 'btn btn-primary btn-sm btn-danger','target'=>'_blank']);
                        }
                    ],

                ],
            ]); ?>

            <div id="btn-submit" class="box-footer text-center">
                <?= Html::button('<i class="fa fa-exchange"></i> ' . Yii::t('app', 'Compare'), ['id' => 'btnCompare', 'class' => 'btn btn-success', 'value' => (string)$model->_id]) ?>
            </div>

            <div id="filter-loading" ></div>

            <div id="gridCompare" style="display:none">
                <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProviderCompare,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // '_id',
                        // 'id',
                        [
                            'attribute' => 'group_id',
                            'value' => function ($model) {
                                return $model->getGroup();
                            }
                        ],
                        [
                            'attribute' => 'testee',
                            'value' => function ($model){
                                return $model->getTestee();
                            }
                        ],
                        [
                            'attribute' => 'tester',
                            'value' => function ($model){
                                return $model->getTester();
                            }
                        ],
                        [
                            'label'=>'Treshold',
                            'format'=>'raw',
                            'value' => function($model){
                                return $model->getTreshold();

                            }
                        ],
                        'score',
                        [
                            'label'=>'Plagiarism',
                            'format'=>'raw',
                            'value' => function($model){
                                return ($model->getTreshold() <= $model->score) ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>';

                            }

                        ],
                        [
                            'class'    => 'yii\grid\ActionColumn',
                            'header'   => 'Diff',
                            'template' => '{diff}',
                            'buttons'  => [

                                'diff' => function ($url, $model) {
                                    $url = Url::toRoute(['group/diff', 'id' => (string)$model->_id, 'tester_id' => (string)$model->tester, 'class_id' => (string)$model->group_id]);

                                    return Html::a(' <span class="glyphicon glyphicon-eye-open" title = "Tooltip Name" ></span> ', 'javascript:void(0)', ['class' => 'list', 'value' => $url]);
                                },
                            ],
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'id'     => "modal",
    'header' => '<h3>Diff</h3>',
    'size' => 'modal-lg',
]);
?>
<div id='modalContent'></div>
<?php
Modal::end();
?>
<?php
$js = <<< JS
    
    $("#btnCompare").click(function(){
        var id = $(this).val();

        $.ajax({
            type: "GET",
            url: "compare",
            data: "id="+id,
            beforeSend: function() {
                $('#filter-loading').html("<img src='/images/load.gif'>");
             },
            success: function (data) {
                console.log(data);
                if(data.result === 'true'){
                    $('#filter-loading').html('');
                    $("#gridCompare").show();
                    $("#w1").yiiGridView("applyFilter");
                }
            },
            dataType: "json"
        });
    });

    $('.list').click(function(e){
        e.preventDefault(); //for prevent default behavior of <a> tag.
        $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));

    });

JS;

$this->registerJs($js);

?>
<?php Pjax::end(); ?>
