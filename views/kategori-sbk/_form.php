<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KategoriSbk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kategori-sbk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'kode_status_aktif') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
