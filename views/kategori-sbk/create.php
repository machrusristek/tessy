<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KategoriSbk */

$this->title = 'Create Kategori Sbk';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Sbk', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-sbk-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
