<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSbkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori Sbk';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-sbk-index">

   <div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">

    <p>
        <?= Html::a('Create Kategori Sbk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'nama',
            'kode_status_aktif',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
</div>
</div>
