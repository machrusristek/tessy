<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KategoriSbk */

$this->title = 'Update Kategori Sbk: ' . $model->_id;
$this->params['breadcrumbs'][] = ['label' => 'Kategori Sbks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_id, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kategori-sbk-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
