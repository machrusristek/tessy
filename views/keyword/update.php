<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Keyword */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Keyword',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Keywords'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="keyword-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
