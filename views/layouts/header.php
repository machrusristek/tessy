<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$image = Yii::getAlias('@webroot') . '/uploads/profile/'.Yii::$app->user->id . '/main.jpg';
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">gtPla</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
<!--                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope-o"></i>
                        <span class="label label-success">4</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 4 messages</li>
                    </ul>
                </li>-->
               
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                         <?php if(Yii::$app->user->can('superadmin')){?>
                        <img src="<?= $directoryAsset.Yii::$app->params['imageUrl']?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <?php }else{ if(file_exists($image)){ ?>
                        <img src="<?= Yii::$app->params['imageUrl'].'/uploads/profile/' .Yii::$app->user->id . '/main.jpg'; ?>" class="user-image" width="160px" height="160px" alt="User Image"/>
                        <?php }else{ ?>
                        <img src="<?=Yii::$app->params['imageUrl'];?>/images/user50x50.jpg" class="user-image" width="160px" height="160px" alt="User Image"/>
                        <?php }} ?>
                        <span class="hidden-xs"><?= yii::$app->user->identity->name;?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php if(Yii::$app->user->can('superadmin')){?>
                            <img src="<?= $directoryAsset.Yii::$app->params['imageUrl'] ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                             <?php }else{ if(file_exists($image)){ ?>
                            <img src="<?= Yii::$app->params['imageUrl'].'/uploads/profile/' .Yii::$app->user->id . '/main.jpg'; ?>" class="img-circle" width="160px" height="160px" alt="User Image"/>
                            <?php }else{ ?>
                            <img src="<?=Yii::$app->params['imageUrl'];?>/images/user50x50.jpg" class="img-circle" width="160px" height="160px" alt="User Image"/>
                            <?php }} ?>

                            <p>
                                <?= yii::$app->user->identity->name;?>
                                <small> <?= yii::$app->user->identity->role;?></small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
<!--                            <div class="col-xs-12 text-center">
                                
                            </div>-->
<!--                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>-->
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/site/update-profile" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
<!--                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
    </nav>
</header>
