<?php 
$image = Yii::getAlias('@webroot') . '/uploads/profile/'.Yii::$app->user->id . '/main.jpg';
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php if (Yii::$app->user->can('superadmin')) { ?>
                    <img src="<?= $directoryAsset.Yii::$app->params['imageUrl'] ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                <?php } else { if (file_exists($image)) { ?>
                        <img src="<?= Yii::$app->params['imageUrl'].'/uploads/profile/' . Yii::$app->user->id . '/main.jpg'; ?>" class="img-circle" width="160px" height="160px" alt="User Image"/>
                    <?php } else { ?>
                        <img src="<?=Yii::$app->params['imageUrl'];?>/images/user50x50.jpg" class="img-circle" width="160px" height="160px" alt="User Image"/>
                <?php } } ?>
            </div>
            <div class="pull-left info">
                <p><?= yii::$app->user->identity->name;?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <?php if(Yii::$app->user->identity->role=='Superadmin'){?>
        <?= dmstr\widgets\Menu::widget(
         [
            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Master',
                        'icon' => 'cog',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Program Hibah', 'icon' => 'file-o', 'url' => ['/program-hibah']],
                            ['label' => 'Kategori Sbk', 'icon' => 'file-o', 'url' => ['/kategori-sbk']],
                            ['label' => 'Configuration', 'icon' => 'file-o', 'url' => ['/params']],
                            ['label' => 'User', 'icon' => 'file-o', 'url' => ['/user']],
                        ],
                    ],
                     [
                                'label' => Yii::t('menu', 'Document'), 
                                'icon' => 'book',
                                'url' => '#',
                                'items' => [
                                    ['label' => Yii::t('menu', 'Submit Pengujian'),  'icon' => 'chevron-right', 'url' => ['/literature/create']],
                                    ['label' => Yii::t('menu', 'Submit Pembanding'),  'icon' => 'chevron-right', 'url' => ['/literature/create-pembanding']],
                                    ['label' => Yii::t('menu', 'List'),  'icon' => 'chevron-right', 'url' => ['/literature/index']],
                                    ['label' => Yii::t('menu', 'Similarity Test'),  'icon' => 'chevron-right', 'url' => ['/plagiarism-test/index']],
                                ],
                            ],
                    // [
                    //             'label' => Yii::t('menu', 'Similarity Test'), 
                    //             'icon' => 'list-alt',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => Yii::t('menu', 'Submit'),  'icon' => 'chevron-right', 'url' => ['/plagiarism-test/create']],
                    //                 ['label' => Yii::t('menu', 'List'),  'icon' => 'chevron-right', 'url' => ['/plagiarism-test/index']],
                    //             ],
                    // ],
                    [
                                'label' => 'Adhoc Comparison',
                                'icon' => 'exchange-alt',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Submit', 'icon' => 'chevron-right', 'url' => ['/adhoc/create']],
                                    ['label' => 'List', 'icon' => 'chevron-right', 'url' => ['/adhoc/index']],
                                ],
                            ],
                ],
            ]
        ) ?>
        <?php }elseif(Yii::$app->user->identity->role=='Reviewer'){ ?>
            <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => Yii::t('menu', 'Document'), 
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => Yii::t('menu', 'Submit Pengujian'),  'icon' => 'chevron-right', 'url' => ['/literature/create']],
                            ['label' => Yii::t('menu', 'List'),  'icon' => 'chevron-right', 'url' => ['/literature/index']],
                            ['label' => Yii::t('menu', 'Similarity Test'),  'icon' => 'chevron-right', 'url' => ['/plagiarism-test/index']],
                        ],
            ],
            [
                        'label' => 'Adhoc Comparison',
                        'icon' => 'exchange-alt',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Submit', 'icon' => 'chevron-right', 'url' => ['/adhoc/create']],
                            ['label' => 'List', 'icon' => 'chevron-right', 'url' => ['/adhoc/index']],
                        ],
            ],
                ],
            ]
            ) ?>
        <?php }elseif(Yii::$app->user->identity->role=='Applicant'){ ?>
            <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    
                    [
                                'label' => Yii::t('menu', 'Document'), 
                                'icon' => 'book',
                                'url' => '#',
                                'items' => [
                                    ['label' => Yii::t('menu', 'Submit Pengujian'),  'icon' => 'chevron-right', 'url' => ['/literature/create']],
                                    ['label' => Yii::t('menu', 'List'),  'icon' => 'chevron-right', 'url' => ['/literature/index']],
                                    ['label' => Yii::t('menu', 'Similarity Test'),  'icon' => 'chevron-right', 'url' => ['/plagiarism-test/index']],
                                ],
                    ],
                    [
                                'label' => 'Adhoc Comparison',
                                'icon' => 'exchange-alt',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Submit', 'icon' => 'chevron-right', 'url' => ['/adhoc/create']],
                                    ['label' => 'List', 'icon' => 'chevron-right', 'url' => ['/adhoc/index']],
                                ],
                    ],
                ],
            ]
        ) ?>
        <?php }else{ ?>
            <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Update profile', 'icon' => 'user', 'url' => ['/site/update-profile']],
                ],
            ]
        ) ?>
        <?php } ?>
    </section>

</aside>
