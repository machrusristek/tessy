<?php

use app\assets\AppAsset;
use dmstr\web\AdminLteAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $content string */

AppAsset::register($this);

AdminLteAsset::register($this);
$logo=Yii::$app->params['imageUrl']."images/logo-default.png";
$prmLogo=\app\models\Params::findOne(["name"=>'Logo']);
if($prmLogo){
    $path_logo = Yii::getAlias('@webroot') .'/uploads/params/'. $prmLogo->value;
    if(file_exists($path_logo)){
        $logo=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmLogo->value;
    }
}

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => $logo]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77358322-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-77358322-6');
</script>
    </head>
    <!-- Start of Async Drift Code -->
    <!-- End of Async Drift Code -->
    <body class="skin-purple">

        <?php $this->beginBody() ?>
        
        <?= $content ?>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
