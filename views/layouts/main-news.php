<?php

use app\assets\AppAsset;
use dmstr\web\AdminLteAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $content string */

AppAsset::register($this);

AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-77358322-2', 'auto');
            ga('send', 'pageview');
        </script> -->
    </head>
    <!-- End of Async Drift Code -->
    <body class="skin-purple">
    <div class="navbar-gohajj" id="gohajj-up">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="logo-gohajj-nav">
                    <a href="/home"><img src="/img/logo-navbar-06.png" style="height: 35px; width: auto; float: left;"></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12"></div>
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                
                    <div class="topnav" id="myTopnav">
                      <a href="/home">HOME</a>
                      <a href="/news">NEWS</a>
                <!--      <a href="#contact">CONTACT</a>
                      <a href="#about">ABOUT</a>-->
                      <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
                    </div>
                
            </div>
            
        </div>
    </div>
    <script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
    </script>
        <?php $this->beginBody() ?>
        
        <?= $content ?>

        <?php $this->endBody() ?>
    <!--this line (footer)-->
<div class="parallax-5">
    <div class="block-color-white-5">
        <div class="container">
            <div class="row footer-tessy">
                
                    <div class="col-xs-12 col-md-4 margin-auto">
                        <div class="foother-tessy-img">
                            <img src="/img/tessy-logo-1.png" style="width: 140px; height: auto;">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 tessy-copyright">
                        <p>
                            © TESSY is a product from <strong>BLUECAMP</strong> Team
                            <br>
                            <?= Yii::powered()?>
                            <br>
                            Icon made by <strong>Popcorns Arts</strong> from <a href="http://www.flaticon.com">www.flaticon.com</a>
                        </p>
                    </div>
                
                <div class="col-xs-12 col-md-4 tessy-keepontouch">
                    <p>Keep in touch via <a href="https://facebook.com/id.tessy" target="_blank">Facebook</a>  | <a href="https://twitter.com/id_tessy" target="_blank">Twitter</a>  |  <a href="">Privacy Policy</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
    </body>
</html>
<?php $this->endPage() ?>
