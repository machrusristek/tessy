<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
$route= Yii::$app->controller->route;
//var_dump($expression);exit();
if (Yii::$app->controller->action->id === 'login' || Yii::$app->controller->action->id === 'home' || Yii::$app->controller->action->id === 'validate' || Yii::$app->controller->action->id === 'register' || Yii::$app->controller->action->id === 'request-password-reset') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    app\assets\LpAsset::register($this);
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
    //blog
} elseif($route == 'news/all' || $route == 'news/single'){
    app\assets\LpAsset::register($this);
    echo $this->render(
        'main-news',
        ['content' => $content]
    );
}else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    $logo=Yii::$app->params['imageUrl']."images/logo-default.png";
    $prmLogo=\app\models\Params::findOne(["name"=>'Logo']);
    if($prmLogo){
        $path_logo = Yii::getAlias('@webroot') .'/uploads/params/'. $prmLogo->value;
        if(file_exists($path_logo)){
            $logo=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmLogo->value;
        }
    }

    $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => $logo]);
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77358322-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-77358322-6');
</script>
    </head>
    <!-- Start of Async Drift Code -->
    <!-- End of Async Drift Code -->
    <body class="hold-transition skin-purple-light sidebar-mini">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
