<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Literature */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="literature-view">

    <?php if(strpos($asset->contentType,'image')===false): //Not an image?>
        <iframe src="<?=Url::to(['literature/get', 'id'=>(string)$asset->_id]);?>" width="100%" height="600px"></iframe>
    <?php else: ?>
        <?= Html::img(Url::to(['literature/get', 'id'=>(string)$asset->_id]), ['alt'=>$asset->description]);?>
    <?php endif; ?>

</div>
