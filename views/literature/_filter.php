<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use filsh\yii2\readmore\Readmore;

$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
$search = "$(document).ready(function(){
    $('#modal-status').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title') 
        var href = button.attr('href') 
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
        });
            
});";
$this->registerJs($search);
?>
<?php
Modal::begin([
    'id' => 'modal-status',
    'header' => '<h4 class="modal-title"></h4>',
    'headerOptions'=>['style'=>'background-color: #8A2BE2; height:50px;color:white;'],
    'options'=>['height'=>'800px', 'tabindex' => false,],
]);

echo '...';

Modal::end();
?>
<div class="row">
    <div class="col-md-12">

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Candidates For Analysis</h3>
                <div class="box-body table-responsive">
                    <table class="table table-hover">

                        <tbody><tr bgcolor="whitesmoke">
                                <th style="text-align: center">Check to compare<br><input type='checkbox' name='checkall' onclick='ToggleAll(this);' checked="checked"></th>
                                <th style="text-align: center">Candidate Rank</th>
                                <!--<th>DB ID</th>-->
                                <th >Source</th>
                                <th >Url/Snippet</th>
                                <th style="text-align: center">Similarity Percentage</th>
                                <th style="text-align: center">Similarity <br>(>= <?= $value;?>%)</th>
                                <th style="text-align: center">Detail</th>

                            </tr>
                            <?php $i =1;$no=0;
                            foreach ($results as $result) {?>
                                <tr>
                                    <td style="text-align: center"><input type="checkbox" name="items[]" class="grup" value="<?= (string)$result->_id?>" checked="checked"></td>
                                    <td style="text-align: center"><b>#<?= $i++; ?><b></td>
                                    <td><?= $result->source?></td>
                                    <td><?php if($result->source=='Online sources'){ ?>
                                        <a href="<?= $result->url;?>" target="_blank"><?= $result->formatedurl;?></a>
                                    <?php }else{
                                         echo $result->url;
                                        }
                                         echo '<br><br>';
                                        ?>
                                        <?php
                                            Readmore::begin();
                                            echo $result->snippet;
                                            Readmore::end()
                                         ?>
                                    </td>
                                    <td style="text-align: right"><div id="sim<?= (string)$result->_id;?>"></div></td>
                                    <td style="text-align: center">
                                        <div id="cek<?= (string)$result->_id;?>">
                                        </div>
                                        <div style='display:none;' id="suk<?= (string)$result->_id;?>">
                                            <i style='color:green' class='fa fa-check fa-2x'></i>
                                        </div>
                                        <div style='display:none;' id="gag<?= (string)$result->_id;?>">
                                            <i style='color:red' class='fa fa-times fa-2x'></i>
                                        </div>
                                    </td>
                                    <td style="text-align: center">
                                        <div style='display:none;' id="modal<?= (string)$result->_id;?>">
                                             <?= Html::a('Upload Manual', ['plagiarism-test/upload-manual','id'=>(string)$result->_id], 
                                                [
                                                    'class' => 'btn btn-block btn-primary btn-flat btn-xs',
                                                    'data-toggle' => "modal",
                                                    'data-target' => "#modal-status",
                                                    'data-title' => Yii::t('app', 'Upload manual content'),
                                                ]);
                                                ?> 
                                        </div>    
                                    <div style='display:none;' id="vew<?= (string)$result->_id;?>"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' => (string)$result->_id]), [
                                    'target' => '_blank',
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                    ]); ?></div></td>

                                </tr>
                            <?php $no++;} ?>


                        </tbody></table>
                </div>
            </div>

        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

</div>
