<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\TouchSpin;
use yii\web\JsExpression;
use app\models\Category;
use app\models\Literature;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Literature */
/* @var $form yii\widgets\ActiveForm */

// $count=  Literature::findAll(['author_id'=>Yii::$app->user->identity->id]);
// $jumlah= count($count);

// $teacher = User::arrayTeacher();
// $value= app\models\Params::findOne(['name'=>'Minimum Keyword'])->value;
// $minimal= app\models\Params::findOne(['name'=>'Maximum Submit'])->value;
// $kategori=  Category::arraySelect();
// $this->registerJs("
// $(document).ready(function(){
//     $('#btn-submit').on('click',function(){
//     var myarr = $('#literature-keywords').val();
//     if(myarr.length >= $value) {
//       return true;  
//     }else{
//         alert('Minimum keywords $value');
//         return false;
//     } 
//   });
  
// })      
// ");
// ?>

<div class="literature-form">
    <div class="box box-primary">
        <div class="box-header with-border">
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <?php
            $form = ActiveForm::begin([
                        'type' => ActiveForm::TYPE_HORIZONTAL,
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
            ]);
            ?>

           <?= $form->field($model, 'kd_program_hibah')->widget(\kartik\widgets\Select2::classname(), [
                'data' => app\models\ProgramHibah::arraySelect(),
                'options' => ['placeholder' => Yii::t('app', 'Pilih Program')],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                
            ]) ?>
            <?= $form->field($model, 'judul')->textarea(['rows' => 2]) ?>

            <?=
            $form->field($model, 'tahun_usulan')->widget(TouchSpin::classname(), [
                'options' => ['placeholder' => 'Enter rating 1 to 5...'],
                'pluginOptions' => [
                    'verticalbuttons' => true,
                    'verticalupclass' => '+',
                    'verticaldownclass' => '-',
                    'initval' => date('Y'),
                    'min' => 1990,
                    'max' => date('Y'),
                    'step' => 1,
                ]
            ])
            ?>

            <?=
            $form->field($model, 'tahun_kegiatan')->widget(TouchSpin::classname(), [
                'options' => ['placeholder' => 'Enter rating 1 to 5...'],
                'pluginOptions' => [
                    'verticalbuttons' => true,
                    'verticalupclass' => '+',
                    'verticaldownclass' => '-',
                    'initval' => date('Y'),
                    'min' => 1990,
                    'max' => date('Y'),
                    'step' => 1,
                ]
            ])
            ?>

            <?php
            
            echo $form->field($model, 'keyword')->widget(Select2::classname(), [
                'options' => ['placeholder' => 'Input keywords ...', 'multiple' => true],
                'pluginOptions' => [
                    'tags' => true,
                    'tokenSeparators' => [','],
                    'minimumInputLength' => 2,
                ],
            ])->hint('Separate keyword with comma (,)');
            ?>

            <?= $form->field($model, 'pengarang')->textinput() ?>
            <?= $form->field($model, 'abstrak')->textarea(['rows' => 3]) ?>
            
            <?php                
                //$browser = $_SERVER['HTTP_USER_AGENT'];
                //$browserName = strpos($browser, "Chrome") || strpos($browser, "Firefox") || strpos($browser, "Safari");
                $browser = new app\helper\Browser();
                if( $browser->getBrowser() == Browser::BROWSER_EDGE && $browser->getBrowser() == Browser::BROWSER_IE ) {
                    $preview = false;
                }else{
                    $preview = true;
                }
                
            ?>
            <?= $form->field($model, 'file')->widget(kartik\widgets\FileInput::classname(), [
                'options' => ['accept' => 'application/pdf'],
                'pluginOptions' => [
                    'showPreview' => $preview,
                    'showRemove' => false,
                    'showUpload' => false,
                    
                ],
            ]); ?>

            <div class="form-group field-literature-citation">
                <label class="control-label col-md-2" for="literature-citation"></label>
                <div class="col-md-10">
                    <div class="help-block">
                        <?= $model->isNewRecord ? '' : '<div style="color:#ff2d2d">leave it blank if no new file is updated</div>' ?>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <center>
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['id'=>'btn-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </center>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    
</div>
