<?php

use yii\helpers\Html;
use filsh\yii2\readmore\Readmore;
use yii\bootstrap\Modal;
?>
<div class="row">
    <div class="col-md-12">

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Literature</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <strong><i class="fa fa-book margin-r-5"></i> Pengarang</strong>
                 
                    <p class="text-muted">
<?= $model->pengarang; ?>
                    </p>
                    <strong><i class="fa fa-book margin-r-5"></i> Judul</strong>

                    <p class="text-muted">
<?= $model->judul; ?>
                    </p>
                    <!--<hr>-->
                    <strong><i class="fa fa-tags margin-r-5"></i> Kata Kunci</strong>

                    <p>
<?= $model->keyword_tags; ?>

                    </p>
                    <!--<hr>-->

                    <strong><i class="fa fa-tags margin-r-5"></i> Abstrak</strong>

                    <p><?php
                        Readmore::begin();
                        echo $model->abstrak;
                        Readmore::end()
                        ?></p>
                    <?php
//                    $file_url = Yii::$app->request->hostInfo. '/uploads/' . $model->file;
//                    $file_location = Yii::$app->basePath. '/web/uploads/' . $model->file;
//                    if (file_exists($file_location)) {
                        ?>

                       

                         <?php $asset = \app\models\Asset::findOne(['literature_id' => (string)$model->_id]); 
                        if($asset){
                        ?>
                         <strong><i class="fa fa-file-pdf-o margin-r-5"></i> File</strong>
                        <p><?= Html::a('Show file', ['literature/file','id'=>(string)$model->_id], ['class' => 'btn btn-primary btn-sm btn-danger','target'=>'_blank']);?> </p>
                        <?php } ?>

                    <?php //  } ?>
                </div>

                <!-- <div class="col-md-6">
                  
                    <strong><i class="fa fa-flag margin-r-5"></i> Language</strong>

                    <p class="text-muted">
<?php //$model->language; ?>
                    </p> -->

                    <!--<hr>-->

                    <strong><i class="fa fa-calendar margin-r-5"></i> Tahun Usulan</strong>

                    <p class="text-muted">
                    <?= $model->tahun_usulan; ?>
                    </p>
                    <strong><i class="fa fa-calendar margin-r-5"></i> Tahun Kegiatan</strong>

                    <p class="text-muted">
                    <?= $model->tahun_kegiatan; ?>
                    </p>

                    <strong><i class="fa fa-tags margin-r-5"></i> Kategori SBK</strong>
                    <p><?php   echo $model->kategori_sbk;  ?></p>
                    <strong><i class="fa fa-tags margin-r-5"></i> Program Hibah</strong>
                    <p><?php   echo $model->programHibah->nama;  ?></p>
                    <strong><i class="fa fa-tags margin-r-5"></i> Jenis Kegiatan</strong>
                    <p><?php   echo $model->jenis_kegiatan;  ?></p>
                </div>

                <?php
//                        Modal::begin([
//                            'id' => 'modal-tester',
//                            'header' => '<h4 class="modal-title">Content</h4>',
//                            'size' => Modal::SIZE_LARGE,
//                            'options' => ['height' => '600px'],
//                            'toggleButton' => ['label' => '<i class="icon fa fa-book"></i> View Content', 'class' => 'btn btn-primary'],
//                        ]);
//
//                        echo $model->content;
//
//                        Modal::end();
                ?>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->

</div>