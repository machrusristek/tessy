<?php

use app\models\Literature;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\Comparison;

/* @var $this View */
/* @var $model Literature */

$this->title = 'Plagiarism Test Result';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literature'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$method = \app\models\PlagiarismCompare::getCollection()->distinct('source');
?>
<div id="printableArea">
    <div style="background-color: white; width:800px; height:600px; padding:20px; text-align:center; border: 10px solid #787878">
        <div style="width:750px; height:550px; padding:20px; text-align:center; border: 5px solid #787878">
            <span style="font-size:30px; font-weight:bold"><?= Yii::$app->name; ?> Certificate</span><hr>
            <span style="font-size:15px">This is to certify that literature</span>
            <br><br>
            <span style="font-size:20px"><i><b>"<?= $model->judul; ?>"</b></i></span><br/><br/>
            <span style="font-size:15px">from author</span> <br/>
            <span style="font-size:20px"><i><b><?= $model->author->name;?></b></i></span> <br/>
            <span style="font-size:15px">in category</span><br/><br/>
            <span style="font-size:15px">has completed the test with result <b><?= count(\app\models\PlagiarismCompare::findAll(['literature_id'=>(string)$model->_id]));?></b> literature[s] found similar using plagiarism test method <b><?= implode(',', $method) ?></b></span> <br/>
            <span style="font-size:15px">Detail result test can be accessed from link below</i></span><br>
            <img src="<?= Url::to(['literature/qrcode', 'id' => (string)$model->_id]); ?>" /><hr>
            <span style="font-size:15px">Tested and issued by <?= 'Supervisor' ?> on <?= date('d M Y', strtotime($model->update_time)); ?></i></span><br>
        </div>
    </div>
</div>
<hr>
<div class="center-block">
    <?=
    Html::a('<i class="fa fa-print"></i> ' . Yii::t('app', 'Print Certificate'), ['plagiarism-certificate', 'id' => (string)$model->_id], [
//        'target' => '_blank',
        'data-toggle' => 'tooltip',
        'class' => 'btn btn-primary',
        'onclick' => "printDiv('printableArea')",
            ]
    )
    ?>     
</div>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>