<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Literature */

$this->title = Yii::t('app', 'Submit Literature');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="literature-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
