<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\models\Category;
use app\models\Params;
use kartik\dialog\Dialog;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LiteratureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Literature');
$this->params['breadcrumbs'][] = $this->title;
echo Dialog::widget();
$js = 'setInterval(
    function refresh()
    {
        $.pjax.reload({
        container:"#my-grid-pjax",
        timeout:false,
    })
    },30000)';
$this->registerJs($js);
?>
<div class="literature-index">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php if(Yii::$app->user->identity->role == 'Superadmin'){?>
    <?php Pjax::begin(['id' => 'my-grid-pjax']); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                
            [
                'attribute' => 'jenis_kegiatan',
                'format' => 'html',
                'hAlign' => 'center',
                'filter' => ['PENELITIAN' => 'PENELITIAN','PENGABDIAN' => 'PENGABDIAN'],
                'value' => function ($model, $key, $index, $widget) {
            return $model->jenis_kegiatan;
            },
            ],
            'tahun_kegiatan',
             'judul',
             'pengarang',
             [
                 'attribute' => 'plagiarism_check_status',
                 'format' => 'html',
                 'hAlign' => 'center',
                 'filter' => ['Submit' => 'Submit','Processing' => 'Processing', 'Tested' => 'Tested'],
                 'value' => function ($model, $key, $index, $widget) {
             return "<span class='label label-warning'>$model->plagiarism_check_status</span>";
             },
             ],
             [
                'attribute' => 'plagiarism_result',
                'format' => 'html',
                'hAlign' => 'center',
                'filter' => ['Plagiat'=>'Plagiat','Not Plagiat'=>'Not Plagiat'],
                'value' => function ($model, $key, $index, $widget) {
            return $model->plagiarism_result?$model->plagiarism_result:"";
            },
            ],
            [
               'attribute' => 'pembanding',
               'format' => 'html',
               'hAlign' => 'center',
               'filter' => ['Y' => 'Y','N' => 'N'],
               'value' => function ($model, $key, $index, $widget) {
           return $model->pembanding;
           },
           ],
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}{delete}{test}{result}{certificate}',
            'header' => '',
                'buttons' => [
                    'result' => function ($url, $model) {
                        if ($model->plagiarism_check_status == 'Tested' && $model->pembanding=='N') {
                            $test=  app\models\PlagiarismTest::find()->where(['document_id'=>(string)$model->_id])->orderBy(['datetime'=>SORT_DESC])->one();
                            if($test){
                            $url = Url::toRoute(['plagiarism-result', 'id' => (string)$test->_id]);
                            return Html::a('View Result', $url, [
                                        'title' => Yii::t('yii', 'View Result'),
                                        'class' => 'btn btn-block btn-default btn-flat btn-xs',
                            ]);
                            } else {
                                return '';
                            } 
                        } else {
                            return '';
                        }
                    },
                              'view' => function ($url, $model) {
                        $url = Url::toRoute(['view', 'id' => (string)$model->_id]);
                        return Html::a('View', $url, [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]);
                    },
                            'update' => function ($url, $model) {

                        $url = Url::toRoute(['update', 'id' => (string)$model->_id]);
                        return Html::a('Update', $url, [
                                    'title' => Yii::t('yii', 'Update'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]);
                    },
                            'delete' => function ($url, $model) {
                        $url = Url::toRoute(['delete', 'id' => (string)$model->_id]);
                        return Html::a('Delete', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                    'data-confirm' => 'Are you sure you want to delete this literature, all related data will be lost?',
                                    'data-method' => 'POST'
                        ]);
                                
                    },
                        'test' => function ($url, $model) {
                        // $url = Url::toRoute(['/plagiarism-test/create', 'id' => (string)$model->_id]);
                        if ($model->plagiarism_check_status != 'Processing' && $model->pembanding=='N') {
                            $label = 'Plagiarism Test';
                            if((string)Yii::$app->user->identity->id==$model->author_id){
                            $id=(string)$model->_id;
                            $url = Url::toRoute(['/plagiarism-test/create', 'id' => $id]);
                            return Html::a($label,$url, [
                                        'title' => Yii::t('yii', $label),
                                        'class' => 'btn btn-block btn-default btn-flat btn-xs']);
                            }
                        }
                        }
                        ,  'certificate' => function ($url, $model) {
                            if ($model->plagiarism_check_status == 'Tested' && $model->pembanding=='N') {
                            $url = Url::toRoute(['/plagiarism-test/certificate', 'id' => (string)$model->plagiarismTest->_id]);
                            return Html::a('Certificate', $url, [
                                        'title' => Yii::t('yii', 'View Result'),
                                        'class' => 'btn btn-block btn-default btn-flat btn-xs',
                            ]);
                        } else {
                            return '';
                        }
                    },
                    ],
        ],]
    ]); ?>
<?php Pjax::end(); ?>
<?php }else{?>
<?php Pjax::begin(['id' => 'my-grid-pjax']); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'jenis_kegiatan',
                'format' => 'html',
                'hAlign' => 'center',
                'filter' => ['PENELITIAN' => 'PENELITIAN','PENGABDIAN' => 'PENGABDIAN'],
                'value' => function ($model, $key, $index, $widget) {
            return $model->jenis_kegiatan;
            },
            ],
            'tahun_kegiatan',
             'judul',
             'pengarang',
             [
                 'attribute' => 'plagiarism_check_status',
                 'format' => 'html',
                 'hAlign' => 'center',
                 'filter' => ['Submit' => 'Submit','Processing' => 'Processing', 'Tested' => 'Tested'],
                 'value' => function ($model, $key, $index, $widget) {
             return "<span class='label label-warning'>$model->plagiarism_check_status</span>";
             },
             ],
             [
                'attribute' => 'plagiarism_result',
                'format' => 'html',
                'hAlign' => 'center',
                'filter' => ['Plagiat'=>'Plagiat','Not Plagiat'=>'Not Plagiat'],
                'value' => function ($model, $key, $index, $widget) {
            return $model->plagiarism_result?$model->plagiarism_result:"";
            },
            ],
        //     [
        //        'attribute' => 'pembanding',
        //        'format' => 'html',
        //        'hAlign' => 'center',
        //        'filter' => ['Y' => 'Y','N' => 'N'],
        //        'value' => function ($model, $key, $index, $widget) {
        //    return $model->pembanding;
        //    },
        //    ],
            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}{delete}{test}{result}{certificate}',
            'header' => '',
                'buttons' => [
                    'result' => function ($url, $model) {
                        if ($model->plagiarism_check_status == 'Tested' && $model->pembanding=='N') {
                            $test=  app\models\PlagiarismTest::find()->where(['document_id'=>(string)$model->_id])->orderBy(['datetime'=>SORT_DESC])->one();
                            if($test){
                            $url = Url::toRoute(['plagiarism-result', 'id' => (string)$test->_id]);
                            return Html::a('View Result', $url, [
                                        'title' => Yii::t('yii', 'View Result'),
                                        'class' => 'btn btn-block btn-default btn-flat btn-xs',
                            ]);
                            } else {
                                return '';
                            } 
                        } else {
                            return '';
                        }
                    },
                              'view' => function ($url, $model) {
                        $url = Url::toRoute(['view', 'id' => (string)$model->_id]);
                        return Html::a('View', $url, [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]);
                    },
                            'update' => function ($url, $model) {
                        $url = Url::toRoute(['update', 'id' => (string)$model->_id]);
                        return Html::a('Update', $url, [
                                    'title' => Yii::t('yii', 'Update'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]);
                    },
                            'delete' => function ($url, $model) {
                                if ($model->plagiarism_check_status == 'Submit') {
                        $url = Url::toRoute(['delete', 'id' => (string)$model->_id]);
                        return Html::a('Delete', $url, [
                                    'title' => Yii::t('yii', 'Delete'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                                    'data-confirm' => 'Are you sure you want to delete this literature, all related data will be lost?',
                                    'data-method' => 'POST'
                        ]);
                        }
                    },
                    'test' => function ($url, $model) {
                        // $url = Url::toRoute(['/plagiarism-test/create', 'id' => (string)$model->_id]);
                        if ($model->plagiarism_check_status != 'Processing' && $model->pembanding=='N') {
                            $label = 'Plagiarism Test';
                            if((string)Yii::$app->user->identity->id==$model->author_id){
                                $id=(string)$model->_id;
                                $url = Url::toRoute(['/plagiarism-test/create', 'id' => $id]);
                                return Html::a($label,$url, [
                                            'title' => Yii::t('yii', $label),
                                            'class' => 'btn btn-block btn-default btn-flat btn-xs']);
                                }
                        }
                        }
                        ,  'certificate' => function ($url, $model) {
                            if ($model->plagiarism_check_status == 'Tested' and $model->plagiarismTest && $model->pembanding=='N') {
                            $url = Url::toRoute(['/plagiarism-test/certificate', 'id' => (string)$model->plagiarismTest->_id]);
                            return Html::a('Certificate', $url, [
                                        'title' => Yii::t('yii', 'View Result'),
                                        'class' => 'btn btn-block btn-default btn-flat btn-xs',
                            ]);
                        } else {
                            return '';
                        }
                    },
                    ],
        ],]
    ]); ?>
<?php Pjax::end(); ?>
<?php }?>
            </div>
            </div>
            </div>
            </div>
            </div>
