<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Literature */

$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Plagiarism Test', 'url' => ['view', 'id' => (string) $model->_id]];
$js = "$(document).ready(function(){
    $('#literature-test_method').change(function () {
    var checked = $(\"input[type='checkbox']:checked\").val();
     if (checked == 'google' || checked == 'tessy' || checked == 'submission') {
            $('#btn-search').show();   
        }else{ 
            $('#btn-search').hide(); 
        } 
    });
        
});";
$this->registerJs($js);
 $value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
?>
<script type='text/javascript'> 
                       checked = false;
                        function ToggleAll(source) {
                        checkboxes = document.getElementsByName('items[]');
                        for(var i=0, n=checkboxes.length;i<n;i++) {
                        checkboxes[i].checked = source.checked;
                        }
                        }
</script>
<div class="literature-view">

    
<?= $this->render('_view', ['model' => $model]) ?>
</div>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Plagiarism Test</h3>
    </div>

    <div class="box-body">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-bell"></i> Notes!</h4>
                    <p>Tessy use pre-filtered algorithm based on keywords to get the most relevant literatures to compare.</p>
                <p>User may choose any sources to compare with your literature.</p>
        </div>
        <?php
        $form = ActiveForm::begin([
                    'type' => ActiveForm::TYPE_HORIZONTAL,
        ]);
        ?>

        <?= $form->field($model, '_id', ['template' => '{input}'])->hiddenInput(['id' => 'literature-id']); ?>

        <?= $form->field($model, 'category_id', ['template' => '{input}'])->textInput(['id' => 'category-id', 'style' => 'display:none']); ?>
        
       <?= $form->field($model, 'test_method[]')->checkboxList(['submission' => 'Submission', 'google' => 'Online sources', 'tessy' => 'Tessy network']); ?> 

        <div id="btn-search" style="display: none">
            <?= Html::button('<i class="fa fa-search"></i> Search for Candidates', ['class' => 'btn btn-success',
                'onclick' => "
                    
                                $.ajax({
                                    url: '" . Url::to(['plagiarism-filter']) . "',
                                    type: 'GET',
                                    data:{
                                        id: $(\"input[name='Literature[_id]']\").val(),
                                        test_method: $(\"input[type='checkbox']:checked\").map(function(){return $(this).val()}).get()
                                    },
                                    dataType: 'html',
                                    async: false,
                                    success: function(data, textStatus, jqXHR, candidates)
                                    {
                                       $('#similar-id').html(data);
                                       $('#btn-submit').show();
                                    },
                                    beforeSend: function()
                                    {
                                       var ajax_load = \"<img src='/images/load.gif' alt='Please wait while filtering...' />\";
                                       $('#similar-id').html(ajax_load);
                                    }
                                });
                            "])
            ?>
        </div>
       

    </div>

    <div style="display: none" id="btn-submit">
    <?= Html::button('Start Plagiarism Test', ['class' => 'btn btn-primary',
                'onclick' => "
                  $('input:checkbox:checked.grup').each(function( index ) {
                    var kode = $(this).val();
                    console.log( index + ': ' + kode );
                    $.ajax({
                                    url: '" . Url::to(['plagiarism-check']) . "',
                                    type: 'GET',
                                    data:{
                                        id: $( this ).val(),
                                    },
                                    async: false,
                                    success: function(data)
                                    { 
                                        console.log(data);
                                        var succes = \"<i style='color:green' class='fa fa-check fa-2x'></i>\";
                                        var gagal = \"<i style='color:red' class='fa fa-times fa-2x'></i>\";
                                        if(data == 'FALSE'){
                                            $('#bar'+data.id).html(gagal);
                                        }else{
                                                $('#bar'+data.id).html(data.result+' %');
                                            if(data.result > $value){
                                                $('#Cek'+data.id).html(succes);
                                            }else{
                                               $('#Cek'+data.id).html(gagal);
                                            }
                                        }
                                    },
                                    beforeSend: function()
                                    {
                                       var ajax_load = \"<img src='/images/processing.gif' alt='Please wait while processing...' />\";
                                       $('#Cek'+kode).html(ajax_load);
                                    }
                    });
                  });          
                "])
            ?>
    </div>
     <br>
    <div id="similar-id"></div>
    
<?php ActiveForm::end(); ?>

</div>

</div>
