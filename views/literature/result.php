<?php

use app\models\Literature;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\PlagiarismTest;
use kartik\widgets\ActiveForm;

/* @var $this View */
/* @var $google Literature */

$this->title = $model->author->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
function convInfNan($val){
  if(is_nan($val) || is_infinite($val)){
      $val =0; 
  }
  return $val;
}
?>
<?= $this->render('_view', ['model' => $model]) ?>
<?php if(Yii::$app->user->identity->role == 'Reviewer'){?>
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Set Status</h3>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
    <?php $form = ActiveForm::begin([
                        'type' => ActiveForm::TYPE_HORIZONTAL,
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
            ]);
            ?>
    
    <div class="col-md-6">
    <?= $form->field($model, 'plagiarism_result')->radioList(['Plagiat'=>'Plagiat','Not Plagiat'=>'Not Plagiat'], ['inline' => true])->label(False);?>
            </div>
    <div class="col-md-6">
                <?= Html::submitButton(Yii::t('app', 'Save'), ['id'=>'btn-submit','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php } ?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Plagiarism Result</h3>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        <div class="callout callout-info">
            <h4><i class="icon fa fa-warning"></i> Attention!</h4>

            <p>Based on plagiarism test and analysis algorithm. TESSY suggests the similarity among your article with the tester articles are listed below:</p>
        </div>
  
  <?php $submissions=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$model->_id,'source'=>'Submission','plagiarism_test_id'=>(string)$test->_id])->all();
  if($submissions){ ?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Submission</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br> (>= <?= $value;?> %)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $ii=1;$total=0;
                  foreach ($submissions as $submission) {   
                    $total += $submission->similarity;     
                ?>
                <tr>
                  <td><?= $ii; ?></td>
                  <td><?= $submission->title?></td>
                  <td><?= $submission->url; ?></td>
                  <td style="text-align: center"><?= number_format(convInfNan($submission->similarity), 2); ?>%</td>
                  <td style="text-align: center"><?= $submission->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' => (string)$submission->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $ii++; } ?>
                <tr bgcolor='whitesmoke'>
                  <td colspan="3"> <b>Rata-rata</b></td>
                  <td style="text-align: center"><?= $total > 0?number_format($total/$ii, 2)."%":0;?></td>
                  <td></td>
                  <td></td>
                </tr>

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>      
       
        
        <p>This report provides results of literature similarity assessment. If the results show unusually high percentage of similarity according to your institution's standard, your supervisor(s) or ethic committee may re-examine your literature.</p>
        <?php // if ($google->plagiarism_check_status == 'Processed' && $google->result_note != '') { ?>
<!--            <p>Supervisor Note:<br>
                <em>"<?php // echo $google->result_note; ?>"</em>
            </p>-->
        <?php // } ?>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">

    </div>

</div>
