<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Literature */

$this->title = Yii::t('app', 'Update {modelClass} ', [
    'modelClass' => 'Literature',
]) ;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->author->name , 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="literature-update">

    <?= $this->render('_form', [
        'model' => $model,
        'asset' => $asset,
    ]) ?>

</div>
