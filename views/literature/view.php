<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\dialog\Dialog;

/* @var $this yii\web\View */
/* @var $model app\models\Literature */

$this->title = 'View for Literature';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->author->name , 'url' => ['view', 'id' => (string) $model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Detail');
echo Dialog::widget();
?>
<div class="literature-view">

    <p>
    <?php if (Yii::$app->user->identity->role == 'Applicant') {?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string) $model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string) $model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    <?php } ?>
    </p>
    
<?= $this->render('_view', ['model' => $model]) ?>
</div>
