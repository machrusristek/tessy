<?php

use yii\bootstrap\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel devmustafa\blog\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container fluid">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 height-line-tessy"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="lastes-news">NEWS</div> 
                </div>
            <?php foreach ($posts as $post) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="news-tessy-container">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 padding-0-tessy">
                            <div class="img-news">
                                <a href="<?= '/news/single?slug=' . $post->getSlug();?>"><img src="<?= $post->getImage();?>"></a>
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 berita-content ">
                            <div class="judul-news"><?= Html::a($post->getTitle(), Url::to(['/news/single?slug=' . $post->getSlug()])) ?></div>
                            <div class="intro-news"><?= $post->getIntro(); ?></div>
                            <div class="date-news"><?= \app\models\Params::DateNews(date('Y-m-d H:i:s', $post->publish_date));?> a go </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
                <div class="col-md-12">
            <?php
            // display pagination
            echo LinkPager::widget([
                'pagination' => $pages,
            ]);
            ?>
        </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <!-------popular-------->
            <div class="popular-news">POPULAR NEWS</div>
             <?php foreach ($popular as $populars) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
                <div class="side-news">
                    <div class="side-img">
                        <a href="<?= '/news/single?slug=' . $populars->getSlug(); ?>"><img src="<?= $populars->getImage(); ?>"></a>
                    </div>
                    <div class="judul-side-news"><?= Html::a($populars->getTitle(), Url::to(['/news/single?slug=' . $populars->getSlug()])) ?></div>
                    <div class="date-news"><?= \app\models\Params::DateNews(date('Y-m-d H:i:s', $populars->publish_date));?> a go </div>
                </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>