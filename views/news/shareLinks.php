<?php
use ijackua\sharelinks\ShareLinks;
use \yii\helpers\Html;

/**
 * @var yii\base\View $this
 */

?>

<div class="socialShareBlock ">
    <?=
    Html::a('<img src="/images/fb.png">', $this->context->shareUrl(ShareLinks::SOCIAL_FACEBOOK),
        ['title' => 'Share to Facebook']) ?>
    <?=
    Html::a('<img src="/images/twitter.png">', $this->context->shareUrl(ShareLinks::SOCIAL_TWITTER),
        ['title' => 'Share to Twitter']) ?>
    <?=
    Html::a('<img src="/images/google.png">', $this->context->shareUrl(ShareLinks::SOCIAL_GPLUS),
        ['title' => 'Share to Google Plus']) ?>
    <?=
    Html::a('<img src="/images/linkedin.png">', $this->context->shareUrl(ShareLinks::SOCIAL_LINKEDIN),
        ['title' => 'Share to LinkedIn']) ?>
    
</div>