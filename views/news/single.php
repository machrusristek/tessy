<?php

use yii\bootstrap\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use kartik\social\Disqus;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $searchModel devmustafa\blog\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $post->getTitle();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['/news']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name' => 'title', 'content' => Html::encode($post->getTitle())]);
$this->registerMetaTag(['name' => 'description', 'content' => Html::encode($post->getIntro())]);
Yii::$app->opengraph->title = $post->getTitle();
Yii::$app->opengraph->description = $post->getIntro();
Yii::$app->opengraph->image = $post->getImage();
?>
<style>
    .body-news{
        color:#000;
        font-size: 16px;
    }
</style>
<div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="lastes-news">NEWS</div> 
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3><?= $post->getTitle();?></h3>
                </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?php echo \ijackua\sharelinks\ShareLinks::widget(
                        [
                            'viewName' => '@app/views/news/shareLinks.php'   //custom view file for you links appearance
                        ]
                         ); ?> 
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-right">
                        <i class='fa fa-eye'/></i> <?= $post->views;?> Views 
                    </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="date-news"><?= \app\models\Params::DateNews(date('Y-m-d H:i:s', $post->publish_date));?> a go </div>
                    <br>
                    <div class="img-single">
                    <img src="<?= $post->getImage(); ?>">
                    </div><br>
                    <div class="body-news"><?= $post->getBody(); ?></div>
                </div>
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <br>
                <?= Disqus::widget(['settings'=>['shortname'=>'tessy-id']]); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <!-------popular-------->
            <div class="popular-news">POPULAR NEWS</div>
            <?php foreach ($popular as $populars) { ?>
                <div class="side-news">
                    <div class="side-img">
                        <a href="<?= '/news/single?slug=' . $populars->getSlug(); ?>"><img src="<?= $populars->getImage(); ?>"></a>
                    </div>
                    <div class="judul-side-news"><?= Html::a($populars->getTitle(), Url::to(['/news/single?slug=' . $populars->getSlug()])) ?></div>
                   <div class="date-news"><?= \app\models\Params::DateNews(date('Y-m-d H:i:s', $populars->publish_date));?> a go </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>