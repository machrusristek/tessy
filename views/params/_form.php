<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Params */
/* @var $form yii\widgets\ActiveForm */
if($model->name=="Logo" || $model->name=="Background"){ 
    $image = Yii::getAlias('@webroot'). '/uploads/params/'.$model->value;
    if(file_exists($image)){
    $img = Html::img(Yii::$app->params['imageUrl'].'/uploads/params/'.$model->value, ['class' => 'file-preview-image img-responsive','width'=>'160px','height'=>'160px']); 
    }else{
        $img = Html::img('', ['class' => 'file-preview-image img-responsive']);
    }
}
?>
<div class="panel panel-default">

<div class="panel-body">
<div class="params-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name') ?>

    <?php if($model->name=="Logo" || $model->name=="Background"){ 
        $hint="";
    if($model->name=="Logo"){
        $hint="max widht : 300 px , max height : 100 px";
    }
    if($model->name=="Background"){
        $hint="widht : 1300 px , height : 600 px";
    }    
    ?>
     <?= $form->field($model, 'value')->widget(FileInput::className(), [
         'options' => [
             'accept' => 'image/*',
            ],
            'pluginOptions' => [
                'showUpload' => false,
                'browseLabel' => Yii::t('app', 'Browse'),
                'removeLabel' => Yii::t('app', 'Delete'),
                'removeClass' => 'btn btn-danger',
                'initialPreview' => $model->isNewRecord ? false : [ $img ],
            ],
            ])->hint($hint) ?>
    <?php }else{ ?>
            <?= $form->field($model, 'value') ?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
