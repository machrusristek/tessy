<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Params */

$this->title = Yii::t('app', 'Create Configuration');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Params'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
