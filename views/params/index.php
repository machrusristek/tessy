<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ParamsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Configuration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-index">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <!-- <? Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // '_id',
            'name',
            [
                'attribute' => 'value',
                'format'=>'html',
                'value' => function ($model, $key, $index, $widget) {
                    if($model->name=="Logo" || $model->name=="Background"){
                        $value="<img src='".Yii::$app->params['imageUrl']."/uploads/params/".$model->value."' width='100px' height='100px'>";
                    }else{
                        $value=$model->value;
                    }
                    return $value;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update}',]
        ],
    ]); ?>
</div>
</div>
</div>
</div>
</div>
