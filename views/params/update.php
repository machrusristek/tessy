<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Params */

$this->title = Yii::t('app', 'Update Configuration: ' . $model->name, [
    'nameAttribute' => '' . $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Configuration'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="params-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
