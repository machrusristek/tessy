<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Params */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Configuration'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="params-view">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'name',
            [
                'label' => 'value',
                'format'=>'html',
                'value' => $model->name=="Logo" || $model->name=="Background"?"<img src='".Yii::$app->params['imageUrl']."/uploads/params/".$model->value."' width='100px' height='100px'>":$model->value,
            ],
        ],
    ]) ?>

</div>
</div>
</div>
</div>
</div>
