<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlagiarismCompare */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plagiarism-compare-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'source') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'url') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'literature_id') ?>

    <?= $form->field($model, 'result') ?>

    <?= $form->field($model, 'snippet') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
