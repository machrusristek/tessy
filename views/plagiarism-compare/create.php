<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlagiarismCompare */

$this->title = Yii::t('app', 'Create Plagiarism Compare');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plagiarism Compares'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plagiarism-compare-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
