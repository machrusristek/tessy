<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PlagiarismCompareSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Plagiarism Compares');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plagiarism-compare-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Plagiarism Compare'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            '_id',
            'source',
            'title',
            'url',
            'content',
            // 'literature_id',
            // 'result',
            // 'snippet',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
