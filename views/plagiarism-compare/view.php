<?php

use app\models\Comparison;
use filsh\yii2\readmore\Readmore;
use yii\bootstrap\Modal;
use yii\helpers\HtmlPurifier;
use yii\web\View;

/* @var $this View */
/* @var $model Comparison */

$this->title = 'Similarity Analysis';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literature'), 'url' => ['/literature']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Comparison Detail</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <h3>Testee</h3>
                    <strong><i class="fa fa-book margin-r-5"></i> Title</strong>

                    <p class="text-muted">
                        <?= $testee->judul; ?>
                    </p>
                    <strong><i class="fa fa-tags margin-r-5"></i> Keywords</strong>

                    <p>
                        <?php foreach (explode(',', $testee->keyword) as $keyword) { ?>
                            <span class="label label-info"><?= trim($keyword); ?></span>
                        <?php } ?>

                    </p>

                    <strong><i class="fa fa-user margin-r-5"></i> Author</strong>

                    <p class="text-muted">
                        <?= $testee->author->name; ?>
                    </p>

                    <strong><i class="fa fa-calendar margin-r-5"></i> Tahun Usulan</strong>

                    <p class="text-muted">
                        <?= $testee->tahun_usulan; ?>
                    </p>


                    <!-- <strong><i class="fa fa-file-text-o margin-r-5"></i> Summary</strong> -->
                    
                    <p><?php
                        // Readmore::begin();
                        // echo $testee->summary;
                        // Readmore::end()
                        ?></p>

                    <p><?php
                        Modal::begin([
                            'id' => 'modal-testee',
                            'header' => '<h4 class="modal-title">Testee Content</h4>',
                            'size' => Modal::SIZE_LARGE,
                            'options' => ['height' => '600px'],
                            'toggleButton' => ['label' => '<i class="icon fa fa-book"></i> View Testee Content', 'class' => 'btn btn-primary'],
                        ]);
                        echo "<pre>".$testee->konten."</pre>";

                        Modal::end();
                        ?>
                    </p>
                </div>
                <div class="col-md-6">
                    <h3>Tester</h3>
                    <strong><i class="fa fa-book margin-r-5"></i> Title</strong>

                    <p class="text-muted">
                        <?= $model->title ?></p>
                    </p>
                    <strong><i class="fa fa-tags margin-r-5"></i> Source</strong>

                    <p>
                        <?php echo $model->source
                // foreach (explode(',', $tester->keywords) as $keyword) { ?>
                            <!--<span class="label label-info"><? trim($keyword); ?></span>-->
                        <?php // } ?>

                    </p>

                    <strong><i class="fa fa-user margin-r-5"></i> Author</strong>

                    <p class="text-muted">
                        <?= $model->author ?>
                    </p>


                    <strong><i class="fa fa-calendar margin-r-5"></i> Tahun Usulan</strong>

                    <p class="text-muted">
                       <?php echo $model->year;?> 
                    </p>


                    <!-- <strong><i class="fa fa-sitemap margin-r-5"></i> URL</strong> -->

                    <!-- <p class="text-muted"> <?php echo $model->url;?></p> -->

                    <!-- <strong><i class="fa fa-file-text-o margin-r-5"></i> Summary</strong> -->
                    
                    <p><?php
//                        Readmore::begin();
//                        echo $model->testee->summary;
//                        Readmore::end()
//                        ?></p>

                    <p><?php
                        Modal::begin([
                            'id' => 'modal-tester',
                            'header' => '<h4 class="modal-title">Tester Content</h4>',
                            'size' => Modal::SIZE_LARGE,
                            'options' => ['height' => '600px'],
                            'toggleButton' => ['label' => '<i class="icon fa fa-book"></i> View Tester Content', 'class' => 'btn btn-primary'],
                        ]);
                        echo "<pre>".$model->content."</pre>";

                        Modal::end();
                        ?>
                    </p>

                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Similarity Analysis</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-bar-chart margin-r-5"></i> Similarity</strong>

                <p class="text-muted">
                    <strong><?= number_format($model->similarity, 2); ?>%</strong> 
                </p>
                <strong><i class="fa fa-copy margin-r-5"></i> Plagiarism Indication</strong> <em>(marked with <i class="text-red">red text</i>)</em>
                <!-- <strong><i class="fa fa-copy margin-r-5"></i> Plagiarism Indication </strong><br><br>
                <p><del style="background:#ffe6e6;">Deleted</del> :The sentence in the red color means that the sentence is appear in the testee document but not appear in the tester document</p>
                <p><ins style="background:#e6ffe6;"s>Inserted</ins> :The sentence in the green color means that the sentence is not appear in the testee document but appear in the tester document</p>
                <p>Plagiat :The sentence in the black color means that the sentence is appear in both document which is indicated as plagiarism </p> -->
                <hr>

                <div class="box box-solid with-border col-md-10">
                    <?= '<pre>'.$model->text_diff.'</pre>'; ?>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<!-- /.col -->

</div>
