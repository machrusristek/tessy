<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\Literature;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use keltstr\simplehtmldom\SimpleHTMLDom;

/* @var $this yii\web\View */
/* @var $model app\models\PlagiarismTest */
/* @var $form yii\widgets\ActiveForm */
$imageUrl=Yii::$app->params['imageUrl'];
// $Literature = Literature::arraySelect();
$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
$js = "$(document).ready(function(){
    if('$id' != ''){
    $.ajax({
                                    url: '" . Url::to(['get-document']) . "',
                                    type: 'GET',
                                    data:{
                                        id: '$id',
                                    },
                                    dataType: 'html',
                                    async: false,
                                    success: function(data)
                                    {
                                       $('#literature-id').html(data);
                                       $('.test-method').show();
                                    }
                                });
    }
    $('#plagiarismtest-document_id').attr('disabled','disabled');

});";
$this->registerJs($js);
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<div class="plagiarism-test-form">
    <div class="row">
        <div class="col-md-12">
        <?= Html::a(Yii::t('app', 'Back'), ['/literature/index'], ['class' => 'btn btn-default']) ?></br></br>
            <!-- About Me Box -->
            
        </div>
    </div>

    <div id="literature-id"></div>

    <div class="box box-success test-method" style="display: none">
        <div class="box-header with-border">
            <h3 class="box-title">Similarity Test</h3>
        </div>

        <div class="box-body">
             <div class="callout callout-info" style="margin-bottom: 0!important;">
                <h4><i class="fa fa-bell"></i> Notes!</h4>
                <p>Tessy uses pre-filtered algorithm that is based on keywords to seek the most relevant literatures to be compared.</p>
                <p>You may compare your uploaded PDF literatures with available sources.</p>
                <p>Tessy will mark every result that equal or above <?= $value;?>% of similarity. It can be adjusted in different version.</p>
             </div>
             <br>
            <div class="col-md-12 col-xs-12" id="btn-submit">
            <center>
              <?= Html::button('Start Similarity Test', ['class' => 'btn btn-success',
                'onclick' => "
                            $.ajax({
                                url: '" . Url::to(['start']) . "',
                                type: 'GET',
                                    data:{
                                        id: '$id',
                                    },
                                async: false,
                                beforeSend: function() {
                                    swal(
                                        {
                                            title:'',
                                            imageUrl: '$imageUrl/images/loading.gif',
                                            text: 'Mohon tunggu ...',
                                            showCancelButton: false,
                                            showConfirmButton: false,
                                            closeOnConfirm: false
                                          }
                                    );
                                },
                                success: function(data)
                                {
                                    swal.close();
                                   $('#similar-id').html(data);
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                                 swal.close();
                                } 
                            });
                "])
            ?>
            </center>
        </div>


        </div>
        <div class="box-body">
        <div id="filter-loading" ></div>
        <div id="similar-id"></div>
        </div>
    </div>

</div>
