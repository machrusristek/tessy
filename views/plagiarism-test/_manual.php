<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Keyword */
/* @var $form yii\widgets\ActiveForm */

$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
?>

<div class="keyword-form">

    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, '_id')->hiddenInput()->label(FALSE) ?>
        <?= $form->field($model, 'content')->textarea(['row'=>15]) ?>

       <?= Html::button('Save', ['class' => 'btn btn-success',
                    'onclick' => "
                                $.ajax({
                                    url: '" . Url::to(['literature/plagiarism-check']) . "',
                                    type: 'GET',
                                    data:{
                                        id: $('#plagiarismcompare-_id').val(),
                                        content: $('#plagiarismcompare-content').val()
                                    },
                                    async: false,
                                    success: function(data)
                                    {
                                      console.log(data);
                                      $('#modal'+data.id).hide();
                                       $('#cek'+data.id).html('');
                                                if(data.result >= $value){
                                                    $('#suk'+data.id).show();
                                                }else{
                                                   $('#gag'+data.id).show();
                                                }
                                                $('#vew'+data.id).show();
                                                $('#sim'+data.id).html(data.result+'%');
                                                $('#modal-status').modal('toggle');
                                        
                                    },
                                });
                            "])
                ?>
    <?php ActiveForm::end(); ?>

</div>
