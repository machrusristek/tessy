<?php

use app\models\Literature;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\Comparison;
use Da\QrCode\QrCode;

/* @var $this View */
/* @var $literature Literature */
$this->title = 'Plagiarism Test Result';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literature'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//generate qrcode
$id=(String)$model->_id;
$qrCode = (new QrCode(Yii::$app->request->hostInfo . '/validate?id=' . $id))
    ->setSize(100)
    ->setMargin(5);
$filepath = Yii::getAlias('@webroot') . '/uploads/qrcode';
$qrCode->writeFile($filepath."/$id.png");

$method = \app\models\PlagiarismCompare::find()->where(['plagiarism_test_id'=>$id])->distinct('source');
$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;   
                    $result= \app\models\PlagiarismCompare::find()
                    ->andFilterWhere([">=","similarity", $value])
                    ->andFilterWhere(["=","plagiarism_test_id", $id])
                    ->count();
?>
<div id="printableArea">
    <div style="background-color: white; width:800px; height:650px; padding:20px; text-align:center; border: 10px solid #787878">
        <div style="width:750px; height:600px; padding:20px; text-align:center; border: 5px solid #787878">
            <span style="font-size:30px; font-weight:bold"><?= Yii::$app->name; ?> Certificate</span><hr>
            <span style="font-size:15px">This is to certify that literature</span>
            <br><br>
            <span style="font-size:20px"><i><b>"<?= $literature->judul; ?>"</b></i></span><br/><br/>
            <span style="font-size:15px">from author</span> <br/>
            <span style="font-size:20px"><i><b><?= $literature->pengarang;?></b></i></span> <br/><br/>
            <span style="font-size:15px">has completed the test with result <b><?= $result;?></b> literature[s] found similar, therefore it's stated as <br>
            <b><?= $literature->plagiarism_result ?></b></span> <br/>
            <span style="font-size:15px">Detail result test can be accessed from link below</i></span><br>
            <img src="<?= Yii::$app->params['imageUrl']."/uploads/qrcode/". $id.".png";?>"><hr>
            <span style="font-size:15px">Tested and issued by <?= 'Supervisor' ?> on <?= date('d M Y', strtotime($model->datetime)); ?></i></span><br>
        </div>
    </div>
</div>
<hr>
<div class="center-block">
    <?=
    Html::a('<i class="fa fa-print"></i> ' . Yii::t('app', 'Print Certificate'), ['certificate', 'id' => (string)$model->_id], [
//        'target' => '_blank',
        'data-toggle' => 'tooltip',
        'class' => 'btn btn-primary',
        'onclick' => "printDiv('printableArea')",
            ]
    )
    ?>     
</div>

<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>