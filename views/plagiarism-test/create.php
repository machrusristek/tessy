<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PlagiarismTest */

$this->title = Yii::t('app', 'New Similarity Test');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Similarity Test'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'New';
?>
<div class="plagiarism-test-create">


    <?= $this->render('_form', [
        'model' => $model,
        'id' => $id,
    ]) ?>

</div>
