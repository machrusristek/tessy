<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PlagiarismTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Similarity Tests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plagiarism-test-index">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'datetime',
//             [
//                'attribute' => 'submit_by',
//                'value' => function ($model, $key, $index, $widget) {
//                    return $model->submit->name;
//                },
//            ],
            [
                'attribute' => 'document_id',
                'label' => 'Title',
                'format'=>'raw',
                'value' => function ($model, $key, $index, $widget) {
                    return $model->literature->judul;
                },
            ],
            [
                'attribute' => 'result',
                'format'=>'raw',
                'value' => function ($model, $key, $index, $widget) { 
                    $value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;   
                    $id=(String)$model->_id;
                    $result= \app\models\PlagiarismCompare::find()
                    ->andFilterWhere([">=","similarity", (int)$value])
                    ->andFilterWhere(["=","plagiarism_test_id", $id])
                    ->all();
                    return Html::a(count($result),['literature/plagiarism-result','id'=>(string)$model->_id]);
                },
            ],
            // 'result_note',

            ['class' => 'yii\grid\ActionColumn',
              'template' => '{certificate}',
              'header' => '',
                'buttons' => [
                   'certificate' => function ($url, $model) {
                            $url = Url::toRoute(['certificate', 'id' => (string)$model->_id]);
                            return Html::a('Certificate', $url, [
                                        'title' => Yii::t('yii', 'View Result'),
                                        'class' => 'btn btn-block btn-info btn-flat btn-xs',
                            ]);
                   },
                ],
                ],
        ],
    ]); ?>

            </div>
            </div>
            </div>
            </div>
            </div>
