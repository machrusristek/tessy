<?php

use app\models\Literature;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use app\models\PlagiarismTest;
use kartik\widgets\ActiveForm;

/* @var $this View */
/* @var $google Literature */

$this->title = $model->author->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Literatures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
?>


<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Plagiarism Result</h3>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        <div class="callout callout-info">
            <h4><i class="icon fa fa-warning"></i> Attention!</h4>

            <p>Based on plagiarism test and analysis algorithm. TESSY suggests the similarity among your article with the tester articles are listed below:</p>
        </div>
<?php $googles=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$model->_id,'source'=>'Online sources','plagiarism_test_id'=>(string)$test->_id])->all();
  if($googles){     
?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Online sources</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                 <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br>(>= <?= $value ;?>%)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $i=1;
                  foreach ($googles as $google) { 
                ?>
                <tr>
                  <td><?= $i; ?></td>
                  <td><?= $google->title?></td>
                  <td><?= $google->url; ?></td>
                  <td style="text-align: center"><?= number_format($google->similarity, 2); ?>%</td>
                  <td style="text-align: center"><?= $google->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' =>(string)$google->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $i++; } ?>
                

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>
  
  <?php $submissions=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$model->_id,'source'=>'Submission','plagiarism_test_id'=>(string)$test->_id])->all();
  if($submissions){ ?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Submission</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                 <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br> (>= <?= $value;?> %)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $ii=1;
                  foreach ($submissions as $submission) {       
                ?>
                <tr>
                  <td><?= $ii; ?></td>
                  <td><?= $submission->title?></td>
                  <td><?= $submission->url; ?></td>
                  <td style="text-align: center"><?= number_format($submission->similarity, 2); ?>%</td>
                  <td style="text-align: center"><?= $submission->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' => (string)$submission->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $ii++; } ?>
                

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>      
        
<?php $tessys=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$model->_id,'source'=>'Tessy network','plagiarism_test_id'=>(string)$test->_id])->all();
  if($tessys){ ?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Tessy network</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br> (>= <?= $value ?>%)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $iii=1;
                  foreach ($tessys as $tessy) {       
                ?>
                <tr>
                  <td><?= $iii; ?></td>
                  <td><?= $tessy->title?></td>
                  <td><?= $tessy->url; ?></td>
                  <td style="text-align: center"><?= number_format($tessy->similarity, 2); ?>%</td>
                  <td style="text-align: center"><?= $tessy->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' => (string)$tessy->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $iii++; } ?>
                

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>      
        
        <p>This report provides results of literature similarity assessment. If the results show unusually high percentage of similarity according to your institution's standard, your supervisor(s) or ethic committee may re-examine your literature.</p>
        <?php // if ($google->plagiarism_check_status == 'Processed' && $google->result_note != '') { ?>
<!--            <p>Supervisor Note:<br>
                <em>"<?php // echo $google->result_note; ?>"</em>
            </p>-->
        <?php // } ?>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">

    </div>

</div>
