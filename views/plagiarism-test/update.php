<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PlagiarismTest */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Plagiarism Test',
]) . $model->_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Plagiarism Tests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_id, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="plagiarism-test-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
