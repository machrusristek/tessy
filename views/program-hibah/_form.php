<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramHibah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-hibah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_program_hibah') ?>
    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'kode_status_aktif') ?>

    <?= $form->field($model, 'kode_jenis_kegiatan') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
