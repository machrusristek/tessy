<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramHibah */

$this->title = 'Create Program Hibah';
$this->params['breadcrumbs'][] = ['label' => 'Program Hibah', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-hibah-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
