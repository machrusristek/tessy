<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramHibahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Hibah';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-hibah-index">

   <div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    <p>
        <?= Html::a('Create Program Hibah', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'kode_program_hibah',
            'nama',
            'kode_status_aktif',
            'kode_jenis_kegiatan',
            'jenis_kegiatan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
</div>
</div>
