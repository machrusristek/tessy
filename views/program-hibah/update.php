<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramHibah */

$this->title = 'Update Program Hibah: ' . $model->_id;
$this->params['breadcrumbs'][] = ['label' => 'Program Hibahs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->_id, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="program-hibah-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
