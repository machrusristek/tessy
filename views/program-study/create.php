<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProgramStudy */

$this->title = Yii::t('app', 'Create Program Study');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Program Study'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-study-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
