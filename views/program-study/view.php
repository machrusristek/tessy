<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\dialog\Dialog;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramStudy */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Program Study'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Dialog::widget();
?>
<div class="program-study-view">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            '_id',
            'name',
        ],
    ]) ?>

</div>
</div>
</div>
</div>
</div>
