<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\Menu;

/** @var dektrium\user\models\User $user */
$user = Yii::$app->user->identity;
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;

?>

    <div class="container">
   <?php if(Yii::$app->user->identity->auth_mode=='TESSY'){?>
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-tabs',
            ],
            'items' => [
                ['label' => Yii::t('user', 'My Profile'), 'url' => ['/site/update-profile']],
                ['label' => Yii::t('user', 'Change Password'), 'url' => ['/site/account']],
//                ['label' => Yii::t('user', 'Networks'), 'url' => ['/user/settings/networks'], 'visible' => $networksVisible],
            ],
        ]) ?>
   <?php }else{ ?> 
         <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-tabs',
            ],
            'items' => [
                ['label' => Yii::t('user', 'My Profile'), 'url' => ['/site/update-profile']],
            ],
        ]) ?>
   <?php } ?> 
    </div>

