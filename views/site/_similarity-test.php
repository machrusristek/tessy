<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\TouchSpin;

/* @var $this yii\web\View */
/* @var $model app\models\Adhoc */
/* @var $form yii\widgets\ActiveForm */

$js = "
    console.log('checked');
    $(\"input[name='SimilarityTest[method]']\").change(function () {

    var checked = $(\"input[name='SimilarityTest[method]']:checked\").val();
    
     if (checked == 'File') {
            $('#file1').show();
            $('#text1').hide();
            $('#file2').show();
            $('#text2').hide();
        } else if (checked == 'Text') {
            $('#file1').hide();
            $('#text1').show();
            $('#file2').hide();
            $('#text2').show();
        }
    $('#btn-submit').show();
    });

";
$this->registerJs($js);

$model = new \app\models\SimilarityTest;
?>

<div class="adhoc-form">

    <div class="box-primary">
        <div class="box-header">
           
        </div>
        <!-- /.box-header -->

        <div class="box-body">

            <?php
            $form = ActiveForm::begin([
                'action' => ['similarity-test'],
                        'options' => [
                            'enctype' => 'multipart/form-data',
                        ],
            ]);
            ?>


            <div class="col-md-6">
                <h3 class="box-title">Testee</h3>
                <hr>
                <?= $form->field($model, 'author1')->textInput(['maxlength' => true]) ?>


            </div>

            <div class="col-md-6">
                <h3 class="box-title">Tester</h3>
                <hr>
                <?= $form->field($model, 'author2')->textInput(['maxlength' => true]) ?>



            </div>
            <div class="col-md-12">
                <div class="text-center" >
<!--                    <=
                    
                    $form->field($model, 'method')->radioButtonGroup([ 'File' => 'Pdf', 'Text' => 'Text',], [
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning']],
                          ])
                    ?>-->
                    <?= $form->field($model, 'method', [
                        'template' => '{label} <div class="row"><div class="col-xs-12">{input}{error}{hint}</div></div>'
                    ])->radioButtonGroup([ 'File' => 'Pdf', 'Text' => 'Text',], 
                        ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning']]]
                    );
                    ?>

<!--                    <php echo $form->field($model, 'method')->radioButtonGroup([ 'File' => 'Pdf', 'Text' => 'Text',], [
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-warning']],

                        'separator'=>false,'template'=>'<div class="item"><div class="col-xs-12">{input}</div><div class="col-xs-12">{label}</div></div>',]);
                    ?>-->
                    
                </div>
            </div>
            <div class="col-md-6">
              <div id="file1" style="display: none">
                  <?= $form->field($model, 'file1')->widget(kartik\widgets\FileInput::classname(), [
                        'options' => ['accept' => 'application/pdf'],
                        'pluginOptions' => [
                            'showRemove' => false,
                            'showUpload' => false,
                            
                        ],
                    ]); ?>
              </div>
              <div id="text1" style="display: none">
                  <?= $form->field($model, 'text1')->textarea(['rows' => 15, 'placeholder' => "Please paste the text"]) ?>
              </div>
            </div>
            <div class="col-md-6">
              <div id="file2" style="display: none">
                  <?= $form->field($model, 'file2')->widget(kartik\widgets\FileInput::classname(), [
                        'options' => ['accept' => 'application/pdf'],
                        'pluginOptions' => [
                            'showRemove' => false,
                            'showUpload' => false,
                            
                        ],
                    ]); ?>
              </div>

              <div id="text2" style="display: none">
                  <?= $form->field($model, 'text2')->textarea(['rows' => 15, 'placeholder' => "Please paste the text"]) ?>
              </div>
            </div>

        </div>

        <div id="btn-submit" class="box-footer text-center" style="display: none">
            <?= Html::submitButton('<i class="fa fa-exchange"></i> ' . Yii::t('app', 'Compare'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
