<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;

/*
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $model dektrium\user\models\SettingsForm
 */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <?= $this->render('_menu') ?>
   
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="col-md-6">
                <?php $form = ActiveForm::begin([
                    'id' => 'account-form',
                ]); ?>

                <?= $form->field($model, 'current_password')->passwordInput() ?>

                <?= $form->field($model, 'new_password')->passwordInput() ?>
                
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>

                <div class="form-group">
                        <?= Html::submitButton('<b>'.Yii::t('user', 'Update').'</b>', ['class' => 'btn btn-block btn-info']) ?><br>
                </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
