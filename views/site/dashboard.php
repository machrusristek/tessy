<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PlagiarismTestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;


use app\models\User;
use app\models\Literature;
use app\models\Asset;
use app\models\PlagiarismTest2;
use app\models\PlagiarismCompare;
use app\models\University;
?>


<?php
    //echo bcdiv($size, 1048576, 6);
    $memberCount = User::find()->where(['university' => Yii::$app->user->identity->university])->count();
    $members = User::find()->where(['university' => Yii::$app->user->identity->university])->all();
    //$members2 = User::find()->where(['university' => Yii::$app->user->identity->university])->all();

    $sumFile = 0;
    foreach ($members as $member) {
        $literatures = Literature::find()->where(['author_id' => (string)$member['_id']])->all();
        foreach ($literatures as $literature) {
            $assets = Asset::find()->where(['literature_id' => (string)$literature['_id']])->all();
            foreach ($assets as $asset) {
                $sumFile +=$asset['length'];
            }
        }
    }

    foreach ($members as $value) {
        $PlagiarismTest += PlagiarismTest2::find()->where(['submit_by' => $value['id']])->count();
        

    }
    

    $university = University::find()->where(['name' => Yii::$app->user->identity->university])->one();
    
    if($university['premium'] == 'Y'){
        $university = 'Premium';
    }else{
        $university = 'Free';
    }
    
?>
<div class="col-sm-12 col-md-12">
    <div class="box box-primary">
        <div class="box-body">
            <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                <b><?= "Dashboard Tessy ". Yii::$app->user->identity->university ?></b>
            </h4>
            <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">
                <b><?= "Status Kampus : ". $university ?></b>
            </h4>
            <section class="content">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

                    <div class="info-box-content">
                        <span>Jumlah registrasi user</span>
                        <span class="info-box-number"><?= $memberCount ?></span>
                    </div>
                    <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-file-pdf-o"></i></span>
        
                    <div class="info-box-content">
                        <span>Jumlah upload file</span>
                        <span class="info-box-number"><?= bcdiv($sumFile, 1048576, 2); ?> MB</span>
                    </div>
                    <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-exchange"></i></span>

                    <div class="info-box-content">
                        <span>Jumlah Proses Komparasi file</span>
                        <span class="info-box-number"><?= $PlagiarismTest; ?></span>
                    </div>
                    <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </section>
        </div>
    </div>
</div>
