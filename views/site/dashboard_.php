<?php
/* @var $this yii\web\View */

$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dashboard'), 'url' => ['index']];
?>


<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">About Tessy</a></li>
                <li><a href="#tab_2" data-toggle="tab">News</a></li>
                <li><a href="#tab_3" data-toggle="tab">Contact</a></li>
                <li><a href="#tab_4" data-toggle="tab">FAQs</a></li>
                <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <blockquote class="pull-right">
                        <small>Tessy 3.0 is a reliable app that has a function in detecting symptom of plagiarism. With three methods of detection such as one on one, mass comparison, and adhoc comparison. Now you can seat relax and make sure that your academic paper has comply with the anti plagiarism rule.</small>
                    </blockquote>
                    <div class="text-center">
                        <img src="/images/106-award.png"></img><br>
                    </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">

                    <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#">Tessy on Daily Social</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    <span class="description">Shared publicly - Nov 23 2015</span>
                  </div>
                  <!-- /.user-block -->
                  <p>
                    TESSY Berupaya Jadi Solusi Isu Plagiarisme untuk Cegah Pencurian Karya Literatur.
                    Mampu mendeteksi kemiripan frase dan memberikan hasil dalam satuan persentase.
                  </p><a href="http://dailysocial.id/post/tessy-berupaya-jadi-solusi-isu-plagiarisme-untuk-cegah-pencurian-karya-literatur">Read more..</a>


                </div>

                    <div class="post">
                  <div class="user-block">
                    <img class="img-circle img-bordered-sm" src="../../dist/img/user1-128x128.jpg" alt="user image">
                        <span class="username">
                          <a href="#">Tessy winning award on 106 Inovasi Indonesia</a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    <span class="description">Shared publicly - Jul 15 2014</span>
                  </div>
                  <!-- /.user-block -->
                  <p>
                    TESSY masuk ke dalam Daftar 106 Inovasi Paling Prospektif 2014 yang telah ditetapkan pada tanggal 14 Juli 2014 di BIC. Penetapan merupakan hasil seleksi ketat yang dilakukan oleh 35 juri BIC, pengusaha, pimpinan perusahaan dari berbagai perusahaan di berbagai sektor.
                  </p><a href="http://bic.web.id/seri-inovasi-indonesia/106-inovasi-2014/259-daftar-106-inovasi-indonesia">Read more..</a>


                </div>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    TESSY is developed by Blue Camp’s Team that comprises:

                    <ul>
                        <li>Didi Achjari</li> 
                        <li>Aman Rohiman</li>
                        <li>Dimas Mukhlas Widiantoro</li> 
                    </ul>

                    TESSY would like to thanks for the support from:
                    <ul>
                        <li>PSDI Universitas Gadjah Mada</li>
                        <li>Faculty of Economics and Business, Universitas Gadjah Mada</li>
                        <li>and Gamatechno</li>
                    </ul>

                    Contacts of future collaborations related to the service can contact: 
                    <ul>
                        <li><a href="mailto:aman.rohiman@gmail.com">aman.rohiman[at]gmail.com</a></li>
                        <li><a href="mailto:dimasmukhlas@gmail.com">dimasmukhlas[at]gmail.com</a></li>
                    </ul>
                </div>
                
                <div class="tab-pane" id="tab_4">
                    Reserved for FAQ
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>


    </div>
    <!-- /.col -->

</div>    
