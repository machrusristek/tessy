<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

   


<div class="row">
    <div>
        <div class="col-xs-12">
            <?php $form = ActiveForm::begin(['id' => 'login-form',
                'enableAjaxValidation'   => false,
                'enableClientValidation' => true,
                'validateOnBlur'         => false,
                'validateOnType'         => false,
                'validateOnChange'       => false,]); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            
                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>
                <div>
                    <div class="form-group col-xs-3">
                        <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                    <div class="col-xs-9">
                        <?=
                        \app\widgets\AuthChoice::widget([
                            'baseAuthUrl' => ['auth'],
                                //'baseAuthUrl' => ['/site/auth'],
                                //                        'popupMode' => false,
                        ])
                        ?>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
            </div>

    </div>
</div>

