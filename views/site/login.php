<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$bg=Yii::$app->params['imageUrl']."/images/background-default.jpg";
$logo=Yii::$app->params['imageUrl']."/images/logo-default.png";
//logo
$prmLogo=\app\models\Params::findOne(["name"=>'Logo']);
if($prmLogo){
    $path_logo = Yii::getAlias('@webroot') .'/uploads/params/'. $prmLogo->value;
    if(file_exists($path_logo)){
        $logo=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmLogo->value;
    }
}

//BG
$prmBg=\app\models\Params::findOne(["name"=>'Background']);
if($prmBg){
    $path_bg = Yii::getAlias('@webroot') .'/uploads/params/'. $prmBg->value;
    if(file_exists($path_bg)){
        $bg=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmBg->value;
    }
}

?>
    

<div id="login-tessy-new"  class="background" style="background-image: url(<?= $bg;?>);" >
<div class="overlay-color">

    <div class="form-box">

        <div class="header">
        <div class="logo-container">
                <img class="logo" src="<?= $logo;?>">
        </div>
        </div>
    

        <div class="content">

            <?php $form = ActiveForm::begin(['id' => 'login-form',
                'enableAjaxValidation'   => false,
                'enableClientValidation' => true,
                'validateOnBlur'         => false,
                'validateOnType'         => false,
                'validateOnChange'       => false,]); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <b><?= Html::a('reset it', ['site/request-password-reset']) ?></b>.
                </div>
                <!-- <div style="color:#999;margin:1em 0">
                    Don't have an account? <a href="/register"><b>Register Here!</b></a>.
                </div> -->
                
                <div>
                    <div class="form-group col-xs-12 btn-tessy">
                        <center>
                        <?= Html::submitButton('Login', ['class' => 'btn btn-primary ', 'name' => 'login-button']) ?>
                        </center>   
                    </div>

                    <!-- <div class="col-xs-12">
                        <=
                        \app\widgets\AuthChoice::widget([
                            'baseAuthUrl' => ['auth'],
                                //'baseAuthUrl' => ['/site/auth'],
                                //                        'popupMode' => false,
                        ])
                        ?>
                    </div> -->
                </div>

            <?php ActiveForm::end(); ?>
            
        </div><!-- content -->

        <div class="footer">
            <img class="logo" src="<?= Yii::$app->params['imageUrl']."/images/gtplagiarismtest.jpeg";?>" >
        </div>

    </div>

</div>
</div>
