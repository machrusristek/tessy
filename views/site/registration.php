<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\University;
use kartik\dialog\Dialog;

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
if($alert){
    echo Dialog::widget();
    }
    
    $search = "$(document).ready(function(){
        krajeeDialog.alert('Please check your email !');
    });";
    $this->registerJs($search);

    $bg=Yii::$app->params['imageUrl']."/images/background-default.jpg";
    $logo=Yii::$app->params['imageUrl']."/images/logo-default.png";
    
    //logo
    $prmLogo=\app\models\Params::findOne(["name"=>'Logo']);
    if($prmLogo){
        $path_logo = Yii::getAlias('@webroot') .'/uploads/params/'. $prmLogo->value;
        if(file_exists($path_logo)){
            $logo=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmLogo->value;
        }
    }
    
    //BG
    $prmBg=\app\models\Params::findOne(["name"=>'Background']);
    if($prmBg){
        $path_bg = Yii::getAlias('@webroot') .'/uploads/params/'. $prmBg->value;
        if(file_exists($path_bg)){
            $bg=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmBg->value;
        }
    }
    
?>

<div id="register-tessy-new"  class="background" style="background-image:  url(<?= $bg;?>);" >
<div class="overlay-color">

    <div class="form-box">

        <div class="header">
            <h3>Register Form</h3>
        </div>
    

        <div class="content">

            <?php $form = ActiveForm::begin([ //'type' => ActiveForm::TYPE_HORIZONTAL,
            ]); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'email') ?> 
            <?= $form->field($model, 'gender')->radioList(['Male' => 'Male', 'Female' => 'Female'], ['inline' => true, 'class' => 'form-tessy-box radio-button-tessy']); ?>
            <?= $form->field($model, 'address')->textarea(['row' => 3, 'class' => 'form-tessy-box']) ?>
            <?= $form->field($model, 'mobile_phone') ?>
            <div class="form-group  btn-tessy">
            <center>
                <?= Html::submitButton( Yii::t('app', 'Register'), ['class' => 'btn btn-primary']) ?>
                <a href="/" class="btn btn-danger">Cancel</a>
            </center>
            </div>

            <?php ActiveForm::end(); ?>

        </div><!-- content -->

        <div class="footer">
            <img class="logo" src="<?= Yii::$app->params['imageUrl']."/images/gtplagiarismtest.jpeg";?>" >
        </div>

    </div>

</div>
</div>




