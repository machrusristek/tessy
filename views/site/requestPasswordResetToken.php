<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request Password Reset';
$this->params['breadcrumbs'][] = $this->title;


$bg="/images/background-default.jpg";
$logo="/images/logo-default.png";

//logo
$prmLogo=\app\models\Params::findOne(["name"=>'Logo']);
if($prmLogo){
    $path_logo = Yii::getAlias('@webroot').'/uploads/params/'. $prmLogo->value;
    if(file_exists($path_logo)){
        $logo='/uploads/params/'.$prmLogo->value;
    }
}

//BG
$prmBg=\app\models\Params::findOne(["name"=>'Background']);
if($prmBg){
    $path_bg = Yii::getAlias('@webroot').'/uploads/params/'. $prmBg->value;
    if(file_exists($path_bg)){
        $bg='/uploads/params/'.$prmBg->value;
    }
}


?>


<div id="login-tessy-new"  class="background" style="background-image: url(<?= $bg;?>);" >
<div class="overlay-color">

    <div class="form-box">

        <div class="header">

            <center><h3><?= Html::encode($this->title) ?></h3></center>
            
        </div>
    

        <div class="content">

            <p>Please fill out your email. A link to reset password will be sent there.</p>

            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

                <?= $form->field($model, 'email') ?>
                <div class="form-group">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                    <a href="/" class="btn btn-danger">Cancel</a>

                </div>

            <?php ActiveForm::end(); ?>

        </div><!-- content -->

        <div class="footer">
            <img class="logo" src="/images/gtplagiarismtest.jpeg">
        </div>

    </div>

</div>
</div>

<div class="site-request-password-reset">
    
</div>
