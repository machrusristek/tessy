<?php

use kartik\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\dialog\DialogAsset;
use yii\bootstrap\Modal;
use kartik\dialog\Dialog;
use yii\widgets\DetailView;
use filsh\yii2\readmore\Readmore;
DialogAsset::register($this);
$search = "$(document).ready(function(){
    console.log('checked');
    $('#modal-status').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title')
        var href = button.attr('href')
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
        });

    BootstrapDialog.alert('Please check your email !');
});";
$this->registerJs($search);
$this->title = Yii::t('app', 'Tessy');

?>


<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Comparison Detail</h3>
                <h5 class="text-center">Comparison method: <strong><?= $model->method; ?></strong></h5>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                    <h3>Testee</h3>
                    <strong><i class="fa fa-user margin-r-5"></i> Author</strong>

                    <p class="text-muted">
                        <?= $model->author1; ?>
                    </p>

                    <strong><i class="fa fa-calendar margin-r-5"></i> Year</strong>

                    <p class="text-muted">
                        <?= $model->year1; ?>
                    </p>


                    <?php if ($model->method == 'File') { ?>
                        <strong><i class="fa fa-sitemap margin-r-5"></i> Filename</strong>

                        <p class="text-muted"><?= $model->file1; ?></p>
                    <?php } ?>
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Content</strong>

                    <p><?php
                        Modal::begin([
                            'id' => 'modal-testee',
                            'header' => '<h4 class="modal-title">Testee Content</h4>',
                            'size' => Modal::SIZE_LARGE,
                            'options' => ['height' => '600px'],
                            'toggleButton' => ['label' => '<i class="icon fa fa-book"></i> View Testee Content', 'class' => 'btn btn-primary'],
                        ]);

                        echo $model->text1;

                        Modal::end();
                        ?>
                    </p>
                </div>
                <div class="col-md-6">
                    <h3>Tester</h3>
                    <strong><i class="fa fa-user margin-r-5"></i> Author</strong>

                    <p class="text-muted">
                        <?= $model->author2; ?>
                    </p>


                    <strong><i class="fa fa-calendar margin-r-5"></i> Year</strong>

                    <p class="text-muted">
                        <?= $model->year2; ?>
                    </p>


                    <?php if ($model->method == 'File') { ?>
                        <strong><i class="fa fa-sitemap margin-r-5"></i> Filename</strong>

                        <p class="text-muted"><?= $model->file2; ?></p>
                    <?php } ?>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Content</strong>

                    <p><?php
                        Modal::begin([
                            'id' => 'modal-tester',
                            'header' => '<h4 class="modal-title">Tester Content</h4>',
                            'size' => Modal::SIZE_LARGE,
                            'options' => ['height' => '600px'],
                            'toggleButton' => ['label' => '<i class="icon fa fa-book"></i> View Tester Content', 'class' => 'btn btn-primary'],
                        ]);

                        echo $model->text2;

                        Modal::end();
                        ?>
                    </p>

                </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Similarity Analysis</h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-bar-chart margin-r-5"></i> Similarity</strong>

                <p class="text-muted">
                    <strong><?= number_format($model->similarity, 2); ?>%</strong> 
                </p>

                <strong><i class="fa fa-copy margin-r-5"></i> Plagiarism Indication</strong> <em>(marked with <i class="text-red">red text</i>)</em>
                <hr>

                <div class="box box-solid with-border col-md-10">
                    <?= $model->diff; ?>
                </div>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
 
