<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use kartik\widgets\FileInput;


$this->title = 'Update Profile';
$this->params['breadcrumbs'][] = $this->title;
$image = Yii::getAlias('@webroot') . '/uploads/profile/'.Yii::$app->user->id . '/main.jpg';
if(file_exists($image)){
   $img = Html::img($model->getMainImage(), ['class' => 'file-preview-image img-responsive','width'=>'160px','height'=>'160px']); 
}else{
    $img = Html::img(Yii::$app->params['imageUrl'].'/images/user50x50.jpg', ['class' => 'file-preview-image img-responsive']);
}
?>
<div class="row">
    <?= $this->render('_menu') ?>
    <div class="panel panel-default">

        <div class="panel-body">
            <?php $form = ActiveForm::begin(['id' => 'form-profile','options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <?= $form->field($model, 'image')->widget(FileInput::className(), [
                    'options' => [
                        'accept' => 'image/*',
                    ],
                    'pluginOptions' => [
                        'showUpload' => false,
                        'browseLabel' => Yii::t('app', 'Browse'),
                        'removeLabel' => Yii::t('app', 'Delete'),
                        'removeClass' => 'btn btn-danger',
                        'initialPreview' => $model->isNewRecord ? false : [ $img ],
                    ],
                ]) ?>
                <?= $form->field($model, 'name')->textInput() ?>
                <?= $form->field($model, 'email')->textInput() ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <?= $form->field($model, 'gender')->radioList(['Male' => 'Male', 'Female' => 'Female'], ['inline' => true, 'class' => 'form-tessy-box radio-button-tessy']) ?>
                <?= $form->field($model, 'address')->textarea(['row' => 3])?>
                <?= $form->field($model, 'mobile_phone')->textInput(['maxlength'=>15]) ?>
                <?=
                    $form->field($model, 'university')->widget(Select2::classname(), [
                        'data' => \app\models\University::arraySelect(),
                        'options' => ['placeholder' => Yii::t('app', 'Choose university')],
                        'pluginOptions' => [
                                'allowClear' => true
                        ],
                    ])
                ?>
                <?=
                                                $form->field($model, 'program_study')->widget(Select2::classname(), [
                                                    'data' => \app\models\ProgramStudy::arraySelect(),
                                                    'options' => ['placeholder' => Yii::t('app', 'Choose Program Study'), 'class' => 'form-tessy-box'],
                                                    'pluginOptions' => [
                                                        'allowClear' => true
                                                    ],
                                                ])
                                                ?>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?= Html::submitButton('Update', ['class' => 'btn btn-block btn-success']) ?>
            </div>
            
            <?php ActiveForm::end(); ?>
            
        </div>
    </div>
</div>

