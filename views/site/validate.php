<?php
use app\models\Literature;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use filsh\yii2\readmore\Readmore;
/* @var $this yii\web\View */

$this->title = Yii::$app->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dashboard'), 'url' => ['index']];
$value= \app\models\Params::findOne(['name'=>'Minimum Result'])->value;
$logo=Yii::$app->params['imageUrl']."/images/logo-default.png";
//logo
$prmLogo=\app\models\Params::findOne(["name"=>'Logo']);
if($prmLogo){
    $path_logo = Yii::getAlias('@webroot') .'/uploads/params/'. $prmLogo->value;
    if(file_exists($path_logo)){
        $logo=Yii::$app->params['imageUrl'].'/uploads/params/'.$prmLogo->value;
    }
}
?>  
<style>
.full-layout {
    max-width: 100%;
    max-height: 100%;
    bottom: 0;
    left: 0;
    margin: auto;
    overflow: auto;
    position: fixed;
    right: 0;
    top: 0;
}
</style>    
    
<div class="parallax-1 full-layout">
    <div class="row margin-0px">
        <div class="block-color-overlay-1 full-layout">
            <div class="container">
                <div class="col-xs-12">
                <center>
            <div class="tessy-logo-header"><img src="<?= $logo;?>" style="width: 100px; height: 100px;"></img></div>
            </center>
<br><br>
<?php if($model){ ?>
<div class="row center">
    <!-- Left col -->
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="box box-info">
            <div class="box-header with-border">
                <h1 class="box-title"><?=Yii::$app->name; ?> Test Result Detail</h1>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-6">
                <strong><i class="fa fa-book margin-r-5"></i> Pengarang</strong>
                 
                 <p class="text-muted">
                <?= $literature->pengarang; ?>
                                </p>
                                <strong><i class="fa fa-book margin-r-5"></i> Judul</strong>

                                <p class="text-muted">
                <?= $literature->judul; ?>
                                </p>
                                <!--<hr>-->
                                <strong><i class="fa fa-tags margin-r-5"></i> Kata Kunci</strong>

                                <p>
                <?= $literature->keyword_tags; ?>

                                </p>
                                <!--<hr>-->

                                <strong><i class="fa fa-tags margin-r-5"></i> Abstrak</strong>

                                <p><?php
                                    Readmore::begin();
                                    echo $literature->abstrak;
                                    Readmore::end()
                                    ?></p>
                </div>

                <div class="col-md-6">
                <strong><i class="fa fa-calendar margin-r-5"></i> Tahun Usulan</strong>

                <p class="text-muted">
                <?= $literature->tahun_usulan; ?>
                </p>
                <strong><i class="fa fa-calendar margin-r-5"></i> Tahun Kegiatan</strong>

                <p class="text-muted">
                <?= $literature->tahun_kegiatan; ?>
                </p>

                <strong><i class="fa fa-tags margin-r-5"></i> Kategori SBK</strong>
                <p><?php   echo $literature->kategori_sbk;  ?></p>
                <strong><i class="fa fa-tags margin-r-5"></i> Program Hibah</strong>
                <p><?php   echo $literature->programHibah->nama;  ?></p>
                <strong><i class="fa fa-tags margin-r-5"></i> Jenis Kegiatan</strong>
                <p><?php   echo $literature->jenis_kegiatan;  ?></p>
                    

                </div>
                
                <?php
//                        Modal::begin([
//                            'id' => 'modal-tester',
//                            'header' => '<h4 class="modal-title">Content</h4>',
//                            'size' => Modal::SIZE_LARGE,
//                            'options' => ['height' => '600px'],
//                            'toggleButton' => ['label' => '<i class="icon fa fa-book"></i> View Content', 'class' => 'btn btn-primary'],
//                        ]);
//
//                        echo $literature->content;
//
//                        Modal::end();
                        ?>
            </div>
            <!-- /.box-body -->
        </div>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Plagiarism Test Result</h3>
    </div>
    <!-- /.box-header -->

    <div class="box-body">
        <div class="callout callout-info">
            <h4><i class="icon fa fa-warning"></i> Attention!</h4>

            <p>Based on plagiarism test and analysis using <?= $literature->test_method ?> methods. <?= Yii::$app->name; ?> suggests the similarity among your article with the articles in our databases are listed below:</p>
        </div>
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
     <?php $googles=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$literature->_id,'source'=>'Online sources','plagiarism_test_id'=>(string)$model->_id])->all();
  if($googles){     
?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Online sources</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                 <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br>(>= <?= $value ;?>%)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $i=1;
                  foreach ($googles as $google) { 
                ?>
                <tr>
                  <td><?= $i; ?></td>
                  <td><?= $google->title?></td>
                  <td><?= $google->url; ?></td>
                  <td style="text-align: center"><?= number_format($google->similarity, 2); ?>%</td>
                  <td style="text-align: center"><?= $google->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' =>(string)$google->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $i++; } ?>
                

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>
  
  <?php $submissions=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$literature->_id,'source'=>'Submission','plagiarism_test_id'=>(string)$model->_id])->all();
  if($submissions){ ?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Submission</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                 <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br> (>= <?= $value;?> %)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $ii=1;
                  foreach ($submissions as $submission) {       
                ?>
                <tr>
                  <td><?= $ii; ?></td>
                  <td><?= $submission->title?></td>
                  <td><?= $submission->url; ?></td>
                  <td style="text-align: center"><?= number_format($submission->similarity, 2); ?>%</td>
                  <td style="text-align: center"><?= $submission->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' => (string)$submission->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $ii++; } ?>
                

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>      
        
<?php $tessys=  app\models\PlagiarismCompare::find()->where(['literature_id'=>(string)$literature->_id,'source'=>'Tessy network','plagiarism_test_id'=>(string)$model->_id])->all();
  if($tessys){ ?>                      
<div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Similar Literatures With Tessy network</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-hover">
                <tbody>
                <tr bgcolor='whitesmoke'>
                  <th>ID</th>
                  <th>Title</th>
                  <th>URL</th>
                  <th style="text-align: center">Similarity</th>
                  <th style="text-align: center">Plagiarism <br> (>= <?= $value ?>%)</th>
                  <th style="text-align: center">Detail</th>
                </tr>
                <?php $iii=1;
                  foreach ($tessys as $tessy) {       
                ?>
                <tr>
                  <td><?= $iii; ?></td>
                  <td><?= $tessy->title?></td>
                  <td><?= $tessy->url; ?></td>
                  <td style="text-align: center"><?= number_format($tessy->similarity, 2); ?>%</td>
                  <td style="text-align: center"><?= $tessy->similarity >= $value ? '<i style="color:green" class="fa fa-check fa-2x"></i>':'<i style="color:red" class="fa fa-times fa-2x"></i>'; ?></td>
                  <td style="text-align: center"><?= Html::a('View', Url::toRoute(['/plagiarism-compare/view', 'id' => (string)$tessy->_id]), [
                                    'title' => Yii::t('yii', 'View'),
                                    'class' => 'btn btn-block btn-default btn-flat btn-xs',
                        ]); ?></td>
                </tr>
                  <?php $iii++; } ?>
                

              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
  <?php } ?>  
            </div>
            <!-- /.box-body -->
          </div>
        <p>This report provides results of literature similarity assessment. If the results show unusually high percentage of similarity according to your institution's standard, your supervisor(s) or ethic committee may re-examine your literature.</p>
        <?php if ($literature->plagiarism_check_status == 'Processed' && $literature->result_note != '') { ?>
            <p>Supervisor Note:<br>
                <em>"<?php echo $literature->result_note; ?>"</em>
            </p>
        <?php } ?>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">

    </div>

</div>
    </div>
    <!-- /.col -->

</div> 
<?php }else{ ?>
            <div class="row center ">
                    <!-- Left col -->
                    <div class="col-xs-12">
                        <div class="box box-warning">
                        <div class="callout callout-danger">
                            <h4><i class="icon fa fa-warning"></i> Error!</h4>
                            <p>Not found!!!</p>
                        </div>
                        </div>
                    </div>
                </div>
<?php  } ?>
 </div>  
 </div>  
 </div>  
 </div>  
 </div>  