<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UniversitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'University');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="university-index">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create University'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'url',
            [   'attribute' => 'active',
                'filter' => ['Y'=>'Y','N'=>'N'],
                'value' => function ($model, $key, $index, $widget) {
                return $model->active;
            },
            ],
            [   'attribute' => 'premium',
                'filter' => ['Y'=>'Y','N'=>'N'],
                'value' => function ($model, $key, $index, $widget) {
                return $model->premium;
            },
            ],
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
 </div>
            </div>
            </div>
            </div>
