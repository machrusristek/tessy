<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\University */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'University',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Universities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="university-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
