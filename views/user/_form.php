<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\University;
if (Yii::$app->controller->action->id === 'update'){
    $btn='Update';
}else{
    $btn='Create';
}
?>

<div class="user-form">
<div class="row">
    <div class="col-md-12">  
        
        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    <?php $form = ActiveForm::begin([
                                'type' => ActiveForm::TYPE_HORIZONTAL,]); ?>
    <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'email') ?> 
            <?= $form->field($model, 'gender')->radioList(['Male' => 'Male', 'Female' => 'Female'], ['inline' => true, 'class' => 'form-tessy-box radio-button-tessy']); ?>
            <?= $form->field($model, 'address')->textarea(['row' => 3, 'class' => 'form-tessy-box']) ?>
            <?= $form->field($model, 'mobile_phone') ?>
            <?= $form->field($model, 'role')->radioList(['Superadmin'=>'Admin','Applicant' => 'Applicant', 'Reviewer' => 'Reviewer'], ['inline' => true, 'class' => 'form-tessy-box radio-button-tessy']); ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
    <div class="form-group">
    <center>
        <?= Html::submitButton(Yii::t('app',$btn), ['class' => 'btn btn-primary']) ?>
    </center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
</div>
</div>
</div>
