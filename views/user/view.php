<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\dialog\Dialog;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo Dialog::widget();
?>
<div class="user-view">
<div class="row">
    <div class="col-md-12">  

        <!-- About Me Box -->
        <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // '_id',
            'username',
            // 'password',
            // 'password_hash',
            // 'password_reset_token',
            'email',
            'role',
            'mobile_phone',
            'gender',
            'address',
            [
                'label' => 'Created At',
                'value' => date('d F Y H:i:s',$model->created_at)
            ],
            [
                'label' => 'Updated At',
                'value' => date('d F Y H:i:s',$model->updated_at)
            ],
        ],
    ]) ?>

</div>
</div>
</div>
</div>
</div>
